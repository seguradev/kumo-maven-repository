<project xmlns="http://maven.apache.org/POM/4.0.0"
	xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
	xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 https://maven.apache.org/xsd/maven-4.0.0.xsd">
	<modelVersion>4.0.0</modelVersion>
	
	<parent>
		<groupId>com.fsegura.kumo-core</groupId>
		<artifactId>kumo-core</artifactId>
		<version>0.0.45-SNAPSHOT</version>
	</parent>
	
	<artifactId>kumo-core-data</artifactId>
	<name>kumo-core-data</name>
	<description>Kumo core data Spring Repositories</description>
	<packaging>jar</packaging>
	
	<properties>
		<query-dsl.generated>${project.build.directory}/generated-sources/java</query-dsl.generated>
	</properties>
	
	<dependencies>
		<!-- SPRING DATA JPA -->
		<dependency>
			<groupId>org.springframework.boot</groupId>
			<artifactId>spring-boot-starter-data-jpa</artifactId>
		</dependency>
		
		<dependency>
			<groupId>io.hypersistence</groupId>
			<artifactId>hypersistence-utils-hibernate-55</artifactId>
			<version>${hypersistence-utils.version}</version>
			<optional>true</optional>
		</dependency>
		
		<!-- QUERYDSL -->
		<dependency>
            <groupId>com.querydsl</groupId>
            <artifactId>querydsl-codegen</artifactId>
            <!-- <version>${querydsl.version}</version> -->
        </dependency>
		<dependency>
			<groupId>com.querydsl</groupId>
			<artifactId>querydsl-jpa</artifactId>
		</dependency>
		<dependency>
			<groupId>com.querydsl</groupId>
			<artifactId>querydsl-collections</artifactId>
		</dependency>
		
		<dependency>
			<groupId>com.fsegura.kumo-core</groupId>
			<artifactId>kumo-core-error</artifactId>
			<version>${project.version}</version>
		</dependency>
		<dependency>
			<groupId>com.fsegura.kumo-core</groupId>
			<artifactId>kumo-core-util</artifactId>
			<version>${project.version}</version>
			<!-- <optional>true</optional> -->
			<exclusions>
				<!-- <exclusion>
					<groupId>com.belike.belike-core</groupId>
					<artifactId>belike-core-data</artifactId>
				</exclusion> -->
				<!-- <exclusion>
					<groupId>com.belike.belike-core</groupId>
					<artifactId>belike-core-data-querydsl</artifactId>
				</exclusion> -->
				<exclusion>
					<groupId>com.belike.belike-core</groupId>
					<artifactId>belike-core-error</artifactId>
				</exclusion>
				<exclusion>
					<groupId>com.belike.belike-core</groupId>
					<artifactId>belike-core-rest-client</artifactId>
				</exclusion>
				<exclusion>
					<groupId>com.belike.belike-core</groupId>
					<artifactId>belike-core-util</artifactId>
				</exclusion>
			</exclusions>
		</dependency>
		
	</dependencies>
	
	<build>
		<plugins>
			<!-- QUERY DSL GENERATOR -->
			<plugin>
				<groupId>com.mysema.maven</groupId>
				<artifactId>apt-maven-plugin</artifactId>
				<dependencies>
                    <dependency>
                        <groupId>com.querydsl</groupId>
                        <artifactId>querydsl-apt</artifactId>
                        <version>${querydsl.version}</version>
                    </dependency>
                    <dependency>
                    	<groupId>com.querydsl</groupId>
                    	<artifactId>querydsl-jpa</artifactId>
                    	<classifier>apt</classifier>
                    	<version>${querydsl.version}</version>
                    </dependency>
                </dependencies>
				<executions>
					<execution>
						<phase>generate-sources</phase>
						<goals>
							<goal>process</goal>
						</goals>
						<configuration>
							<!-- <includes></includes> -->
							<outputDirectory>${query-dsl.generated}</outputDirectory>
							<processors>
								<processor>com.querydsl.apt.jpa.JPAAnnotationProcessor</processor>
								<processor>lombok.launch.AnnotationProcessorHider$AnnotationProcessor</processor>
							</processors>
							<options>
								<querydsl.entityAccessors>true</querydsl.entityAccessors>
								<querydsl.listAccessors>true</querydsl.listAccessors>
								<querydsl.mapAccessors>true</querydsl.mapAccessors>
              					<!-- <querydsl.generatedAnnotationClass>javax.annotation.Generated</querydsl.generatedAnnotationClass> -->
							</options>
						</configuration>
					</execution>
				</executions>
			</plugin>
			
			<!-- BUILD HELPER -->
			<plugin>
				<groupId>org.codehaus.mojo</groupId>
				<artifactId>build-helper-maven-plugin</artifactId>
				<executions>
	        		<execution>
	        			<id>add-source</id>
            			<phase>generate-sources</phase>
            			<goals>
			                <goal>add-source</goal>
			            </goals>
	        		</execution>
	        	</executions>
	        	<configuration>
	        		<sources>
	        			<!-- <source>${mapstruct.generated}</source> -->
	        			<source>${query-dsl.generated}</source>
	        		</sources>
	        	</configuration>
			</plugin>
		</plugins>
	</build>
		
</project>
