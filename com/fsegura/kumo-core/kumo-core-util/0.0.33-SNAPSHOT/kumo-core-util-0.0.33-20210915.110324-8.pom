<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
	<modelVersion>4.0.0</modelVersion>
	
	<parent>
		<groupId>com.fsegura.kumo-core</groupId>
		<artifactId>kumo-core</artifactId>
		<version>0.0.33-SNAPSHOT</version>
	</parent>
	
	<artifactId>kumo-core-util</artifactId>
	<name>kumo-core-util</name>
	<packaging>jar</packaging>
	
	<dependencies>
		<!-- SPRING FRAMEWORK -->
		<dependency>
		    <groupId>org.springframework</groupId>
		    <artifactId>spring-context</artifactId>
		</dependency>
		<dependency>
		    <groupId>org.springframework</groupId>
		    <artifactId>spring-tx</artifactId>
		</dependency>
		<dependency>
		    <groupId>org.springframework.security</groupId>
		    <artifactId>spring-security-core</artifactId>
		</dependency>
		
		<dependency>
			<groupId>org.hibernate</groupId>
  			<artifactId>hibernate-core</artifactId>
  			<version>${hibernate.version}</version>
  			<!-- <exclusions>
  				<exclusion>
  					 <groupId>javax.activation</groupId>
  					 <artifactId>javax.activation-api</artifactId>
  				</exclusion>
  				<exclusion>
  					<groupId>javax.persistence</groupId>
  					<artifactId>javax.persistence-api</artifactId>
  				</exclusion>
  				<exclusion>
  					<groupId>javax.xml.bind</groupId>
  					<artifactId>jaxb-api</artifactId>
  				</exclusion>
  			</exclusions> -->
		</dependency>
		
		<!-- PERSISTENCE-API -->
        <dependency>
    		<groupId>jakarta.persistence</groupId>
    		<artifactId>jakarta.persistence-api</artifactId>
    		<!-- jakarta.jakartaee-bom:8.0.0 tiene mal definida la versión de esta dependencia -->
    		<!-- <version>${jakarta.persistence-api.version}</version> -->
		</dependency>
		
		<dependency>
		    <groupId>jakarta.xml.bind</groupId>
		    <artifactId>jakarta.xml.bind-api</artifactId>
		</dependency>
		
		<!-- Si se utiliza jakarta.jakartaee:9.0.0 -->
		<!-- <dependency>
			<groupId>javax.ws.rs</groupId>
			<artifactId>javax.ws.rs-api</artifactId>
			<version>${javax.ws.rs.version}</version>
		</dependency> -->
		
		<!-- JERSEY CLIENT -->
		<dependency>
    		<groupId>org.glassfish.jersey.core</groupId>
    		<artifactId>jersey-client</artifactId>
		</dependency>
		
		<dependency>
			 <groupId>org.reflections</groupId>
			 <artifactId>reflections</artifactId>
			 <version>${reflections.version}</version>
		</dependency>
		
		<!-- BELIKE-CORE-ERROR -->
		<dependency>
			<groupId>com.belike.belike-core</groupId>
			<artifactId>belike-core-error</artifactId>
		</dependency>
		<!-- BELIKE-CORE-DATA -->
		<dependency>
			<groupId>com.belike.belike-core</groupId>
			<artifactId>belike-core-data</artifactId>
		</dependency>
		<dependency>
			<groupId>com.belike.belike-core</groupId>
			<artifactId>belike-core-data-querydsl</artifactId>
		</dependency>
		<!-- BELIKE-CORE-UTIL -->
		<dependency>
			<groupId>com.belike.belike-core</groupId>
			<artifactId>belike-core-util</artifactId>
		</dependency>
		<!-- BELIKE-CORE-REST-CLIENT -->
		<dependency>
			<groupId>com.belike.belike-core</groupId>
			<artifactId>belike-core-rest-client</artifactId>
		</dependency>
		<!-- BELIKE-CORE-WEBSOCKET -->
		<dependency>
			<groupId>com.belike.belike-core</groupId>
			<artifactId>belike-core-websocket</artifactId>
		</dependency>
		
		<!-- WEBSOCKET -->
		<dependency>
			<groupId>jakarta.websocket</groupId>
    		<artifactId>jakarta.websocket-api</artifactId>
			<scope>provided</scope>
		</dependency>
		
		<!-- JAKARTA WS.RS -->
        <dependency>
        	<groupId>jakarta.ws.rs</groupId>
    		<artifactId>jakarta.ws.rs-api</artifactId>
    		<scope>provided</scope>
		</dependency>
		
		<!-- KUMO-CORE-COMMONS -->
		<dependency>
			<groupId>com.fsegura.kumo-core</groupId>
			<artifactId>kumo-core-commons</artifactId>
			<version>${project.version}</version>
		</dependency>
		
		<dependency>
			<groupId>com.amazonaws</groupId>
			<artifactId>amazon-sqs-java-extended-client-lib</artifactId>
			<version>${amazon-sqs-java-extended-client-lib.version}</version>
		</dependency>
		<dependency>
			<groupId>software.amazon.sns</groupId>
			<artifactId>sns-extended-client</artifactId>
			<version>${amazon-sns-java-extended-client-lib.version}</version>
		</dependency>
		
		<dependency>
			<groupId>com.amazonaws</groupId>
			<artifactId>aws-java-sdk-core</artifactId>
			<version>${aws-java-sdk.version}</version><!--$NO-MVN-MAN-VER$-->
		</dependency>
		<dependency>
			<groupId>com.amazonaws</groupId>
			<artifactId>aws-java-sdk-kms</artifactId>
			<version>${aws-java-sdk.version}</version><!--$NO-MVN-MAN-VER$-->
		</dependency>
		<dependency>
			<groupId>com.amazonaws</groupId>
			<artifactId>aws-java-sdk-sqs</artifactId>
			<version>${aws-java-sdk.version}</version><!--$NO-MVN-MAN-VER$-->
		</dependency>
		<dependency>
			<groupId>com.amazonaws</groupId>
			<artifactId>aws-java-sdk-s3</artifactId>
			<version>${aws-java-sdk.version}</version><!--$NO-MVN-MAN-VER$-->
		</dependency>
		<dependency>
			<groupId>com.amazonaws</groupId>
			<artifactId>aws-java-sdk-sns</artifactId>
			<version>${aws-java-sdk.version}</version><!--$NO-MVN-MAN-VER$-->
		</dependency>
		
		<!-- COMMONS-LANG -->
		<dependency>
		    <groupId>org.apache.commons</groupId>
		    <artifactId>commons-lang3</artifactId>
		    <version>${commons-lang3.version}</version>
		</dependency>
		
		<dependency>
			<groupId>commons-codec</groupId>
			<artifactId>commons-codec</artifactId>
			<version>${commons-codec.version}</version>
		</dependency>
		
		<dependency>
    		<groupId>org.apache.commons</groupId>
    		<artifactId>commons-collections4</artifactId>
    		<version>${commons-collections4.version}</version>
    	</dependency>
		
		<!-- COMMONS-NET -->
		<dependency>
    		<groupId>commons-net</groupId>
    		<artifactId>commons-net</artifactId>
    		<version>${commons-net.version}</version>
    		<scope>compile</scope>
      		<optional>true</optional>
		</dependency>
		
		<!-- JSON SCHEMA V3 -->
		<dependency>
			<groupId>com.fasterxml.jackson.module</groupId>
			<artifactId>jackson-module-jsonSchema</artifactId>
		</dependency>
		
		<!-- JSON SCHEMA V4 -->
		<dependency>
			<groupId>com.kjetland</groupId>
			<artifactId>mbknor-jackson-jsonschema_2.12</artifactId>
			<version>${mbknor-jackson-jsonschema.version}</version>
		</dependency>
	</dependencies>
	
	<build>
		<plugins>
			<plugin>
				<groupId>org.codehaus.mojo</groupId>
				<artifactId>properties-maven-plugin</artifactId>
				<version>${properties-maven-plugin.version}</version>
				<executions>
					<execution>
						<phase>initialize</phase>
						<goals>
							<goal>read-project-properties</goal>
						</goals>
						<configuration>
							<files>
								<file>${basedir}/src/main/resources/core.properties</file>
							</files>
						</configuration>
					</execution>
				</executions>
			</plugin>
			
			<!-- SPOTBUGS MAVEN PLUGIN -->
			<plugin>
				<groupId>com.github.spotbugs</groupId>
				<artifactId>spotbugs-maven-plugin</artifactId>
				<configuration>
					<excludeFilterFile>${basedir}/src/main/resources/spotbugs/spotbugs-exclude.xml</excludeFilterFile>
				</configuration>
			</plugin>
		</plugins>
	</build>
		
</project>