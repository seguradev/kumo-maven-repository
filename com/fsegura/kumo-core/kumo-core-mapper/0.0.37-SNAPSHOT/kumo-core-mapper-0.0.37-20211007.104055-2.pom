<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 https://maven.apache.org/xsd/maven-4.0.0.xsd">
	<modelVersion>4.0.0</modelVersion>
	<parent>
		<groupId>com.fsegura.kumo-core</groupId>
		<artifactId>kumo-core</artifactId>
		<version>0.0.37-SNAPSHOT</version>
	</parent>
	
	<artifactId>kumo-core-mapper</artifactId>
	<name>kumo-core-mapper</name>
	
	<dependencies>
		<!-- SPRING FRAMEWORK -->
		<dependency>
		    <groupId>org.springframework</groupId>
		    <artifactId>spring-context</artifactId>
            <scope>provided</scope>
		</dependency>
		
		<!-- BELIKE-CORE-DATA -->
		<dependency>
			<groupId>com.belike.belike-core</groupId>
			<artifactId>belike-core-data</artifactId>
		</dependency>
		<!-- BELIKE-CORE-MAPPER -->
		<dependency>
			<groupId>com.belike.belike-core</groupId>
			<artifactId>belike-core-mapper</artifactId>
		</dependency>
		
		<!-- KUMO-CORE-COMMONS -->
		<dependency>
			<groupId>com.fsegura.kumo-core</groupId>
			<artifactId>kumo-core-commons</artifactId>
			<version>${project.version}</version>
		</dependency>
		<!-- KUMO-CORE-DAO -->
		<dependency>
			<groupId>com.fsegura.kumo-core</groupId>
			<artifactId>kumo-core-dao</artifactId>
			<version>${project.version}</version>
		</dependency>
		<!-- KUMO-CORE-UTIL -->
		<dependency>
			<groupId>com.fsegura.kumo-core</groupId>
			<artifactId>kumo-core-util</artifactId>
			<version>${project.version}</version>
			<exclusions>
				<exclusion>
					<groupId>com.amazonaws</groupId>
					<artifactId>amazon-sqs-java-extended-client-lib</artifactId>
				</exclusion>
				<exclusion>
					<groupId>software.amazon.sns</groupId>
					<artifactId>sns-extended-client</artifactId>
				</exclusion>
				<exclusion>
					<groupId>com.amazonaws</groupId>
					<artifactId>aws-java-sdk-core</artifactId>
				</exclusion>
				<exclusion>
					<groupId>com.amazonaws</groupId>
					<artifactId>aws-java-sdk-kms</artifactId>
				</exclusion>
				<exclusion>
					<groupId>com.amazonaws</groupId>
					<artifactId>aws-java-sdk-s3</artifactId>
				</exclusion>
				<exclusion>
					<groupId>com.amazonaws</groupId>
					<artifactId>aws-java-sdk-sns</artifactId>
				</exclusion>
				<exclusion>
					<groupId>com.amazonaws</groupId>
					<artifactId>aws-java-sdk-sqs</artifactId>
				</exclusion>
				<exclusion>
					<groupId>com.belike.belike-core</groupId>
            		<artifactId>belike-core-rest-client</artifactId>
				</exclusion>
				<exclusion>
					<groupId>com.belike.belike-core</groupId>
            		<artifactId>belike-core-websocket</artifactId>
				</exclusion>
			</exclusions>
		</dependency>

		<!-- DOZER SPRING BOOT -->
		<dependency>
			<groupId>com.github.dozermapper</groupId>
			<artifactId>dozer-spring-boot-starter</artifactId>
			<version>${dozer.version}</version>
		</dependency>
		
		<!-- MAPSTRUCT -->
		<dependency>
	        <groupId>org.mapstruct</groupId>
	        <artifactId>mapstruct</artifactId>
	        <scope>compile</scope>
      		<optional>true</optional>
    	</dependency>
    	
    	<!-- APACHE COMMONS -->
    	<dependency>
    		<groupId>org.apache.commons</groupId>
    		<artifactId>commons-collections4</artifactId>
    		<version>${commons-collections4.version}</version>
    	</dependency>
	</dependencies>
	
	<build>
		<resources>
			<resource>
				<directory>src/main/resources</directory>
				<filtering>false</filtering>
				<includes>
					<include>**/*.properties</include>
					<include>**/*.xml</include>
				</includes>
			</resource>
		</resources>
		
		<plugins>
			<!-- SPOTBUGS MAVEN PLUGIN -->
			<plugin>
				<groupId>com.github.spotbugs</groupId>
				<artifactId>spotbugs-maven-plugin</artifactId>
				<configuration>
					<excludeFilterFile>${basedir}/src/main/resources/spotbugs/spotbugs-exclude.xml</excludeFilterFile>
				</configuration>
			</plugin>
		</plugins>
	</build>
	
</project>