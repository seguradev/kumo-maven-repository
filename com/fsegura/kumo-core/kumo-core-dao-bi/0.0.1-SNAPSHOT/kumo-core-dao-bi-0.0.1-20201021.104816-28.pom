<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0"
	xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
	xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
	<modelVersion>4.0.0</modelVersion>
	
	<parent>
		<groupId>com.fsegura.kumo-core</groupId>
		<artifactId>kumo-core</artifactId>
		<version>0.0.1-SNAPSHOT</version>
	</parent>
	
	<artifactId>kumo-core-dao-bi</artifactId>
	<name>kumo-core-dao-bi</name>
	<packaging>jar</packaging>
	
	<properties>
		<query-dsl.generated>${project.build.directory}/generated-sources/java</query-dsl.generated>
		<hibernate-ddl.generated>${project.build.directory}/generated-resources/sql/ddl</hibernate-ddl.generated>
	</properties>
	
	<dependencies>
		<!-- SPRING CONFIG PROCESSOR -->
		<dependency>
			<groupId>org.springframework.boot</groupId>
			<artifactId>spring-boot-configuration-processor</artifactId>
			<optional>true</optional>
		</dependency>
		<!-- SPRING DATA JPA -->
		<dependency>
			<groupId>org.springframework.boot</groupId>
			<artifactId>spring-boot-starter-data-jpa</artifactId>
		</dependency>
		
		<!-- BELIKE-CORE-DATA -->
		<dependency>
			<groupId>com.belike.belike-core</groupId>
			<artifactId>belike-core-data-querydsl</artifactId>
		</dependency>
		
		<!-- REDSHFIT -->
		<dependency>
			<groupId>com.amazon.redshift</groupId>
			<artifactId>redshift-jdbc42-no-awssdk</artifactId>
			<version>${redshift.version}</version>
		</dependency>
		<!-- <dependency>
			<groupId>com.amazon.redshift</groupId>
			<artifactId>redshift-jdbc42</artifactId>
			<version>${redshift.version}</version>
		</dependency> -->
				
		<!-- QUERYDSL -->
		<dependency>
			<groupId>com.querydsl</groupId>
			<artifactId>querydsl-jpa</artifactId>
		</dependency>
		<dependency>
			<groupId>com.querydsl</groupId>
			<artifactId>querydsl-collections</artifactId>
		</dependency>
		
		<!-- HIBERNATE-VALIDATOR -->
        <dependency>
            <groupId>org.hibernate.validator</groupId>
            <artifactId>hibernate-validator</artifactId>
        </dependency>
        
        <!-- HIBERNATE EHCACHE -->
		<dependency>
		    <groupId>org.hibernate</groupId>
		    <artifactId>hibernate-ehcache</artifactId>
		</dependency>
	</dependencies>
	
	<profiles>
		<profile>
			<id>local</id>
			<properties>
				<name>env</name>
				<value>local</value>
				<spring.profiles.active>local</spring.profiles.active>
				<!-- <app.filter>src/main/resources/application-local-${user.name}.properties</app.filter> -->
				<!-- <spring.profiles.active>local-${user.name}</spring.profiles.active> -->
			</properties>
			<activation>
				<activeByDefault>true</activeByDefault>
			</activation>
		</profile>
		<profile>
			<id>test</id>
			<properties>
				<name>env</name>
				<value>test</value>
				<spring.profiles.active>test</spring.profiles.active>
			</properties>
		</profile>
		<profile>
			<id>pro</id>
			<properties>
				<name>env</name>
				<value>pro</value>
				<spring.profiles.active>pro</spring.profiles.active>
			</properties>
		</profile>
	</profiles>
	
	<build>
		<extensions>
			<extension>
				<groupId>ar.com.synergian</groupId>
				<artifactId>wagon-git</artifactId>
				<version>${wagon-git.version}</version>
			</extension>
		</extensions>
		
		<resources>
			<resource>
				<directory>src/main/resources</directory>
				<filtering>true</filtering>
				<includes>
					<include>**/*.properties</include>
					<include>**/*.sql</include>
				</includes>
			</resource>
		</resources>
		
		<plugins>
			<!-- HIBERNATE 5.4.x DDL GENERATOR -->
			<plugin>
			   	<groupId>de.jpdigital</groupId>
			   	<artifactId>hibernate54-ddl-maven-plugin</artifactId>
			   	<configuration>
				   	<!-- <dialects>
	                   <param>postgresql9</param>
	               	</dialects> -->
	               	<customDialects>
                    	<param>com.belike.fsegura.kumo.dao.bi.hibernate.dialect.RedshiftDialect</param>
                	</customDialects>
	               	<packages>
	                   	<param>com.belike.fsegura.kumo.dao.bi.entities</param>
	               	</packages>
	               	<outputDirectory>${hibernate-ddl.generated}</outputDirectory>
			   	</configuration>
			</plugin>
			
			<!-- QUERY DSL GENERATOR -->
			<plugin>
				<groupId>com.mysema.maven</groupId>
				<artifactId>apt-maven-plugin</artifactId>
				<!-- <dependencies>
					<dependency>
						<groupId>com.querydsl</groupId>
						<artifactId>querydsl-apt</artifactId>
						<version>${querydsl.version}</version>
					</dependency>
				</dependencies> -->
				<executions>
					<execution>
						<goals>
							<goal>process</goal>
						</goals>
						<configuration>
							<outputDirectory>${query-dsl.generated}</outputDirectory>
							<processor>com.querydsl.apt.jpa.JPAAnnotationProcessor</processor>
							<options>
								<querydsl.entityAccessors>true</querydsl.entityAccessors>
							</options>
						</configuration>
					</execution>
				</executions>
			</plugin>
 			
 			<!-- FLYWAY -->
			<plugin>
				<groupId>org.flywaydb</groupId>
				<artifactId>flyway-maven-plugin</artifactId>
				<!-- <dependencies>
					<dependency>
						<groupId>com.amazon.redshift</groupId>
						<artifactId>redshift-jdbc42-no-awssdk</artifactId>
						<version>${redshift.version}</version>
					</dependency>
				</dependencies> -->
				<configuration>
					<configFiles>
						<configFile>src/main/resources/flyway-${spring.profiles.active}.properties</configFile>
					</configFiles>
				</configuration>
			</plugin>
			
			<!-- MAVEN BUILD HELPER -->
			 <plugin>
	        	<groupId>org.codehaus.mojo</groupId>
	        	<artifactId>build-helper-maven-plugin</artifactId>
	        	<executions>
	        		<execution>
	        			<id>add-source</id>
            			<phase>generate-sources</phase>
            			<goals>
			                <goal>add-source</goal>
			            </goals>
	        		</execution>
	        	</executions>
	        	<configuration>
	        		<sources>
	        			<source>${query-dsl.generated}</source>
	        		</sources>
	        	</configuration>
			</plugin>
		</plugins>
	</build>
</project>