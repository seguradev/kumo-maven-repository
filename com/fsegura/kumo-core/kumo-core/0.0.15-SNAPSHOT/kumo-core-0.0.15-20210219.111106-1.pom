<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 https://maven.apache.org/xsd/maven-4.0.0.xsd">
	<modelVersion>4.0.0</modelVersion>
	
	<groupId>com.fsegura.kumo-core</groupId>
	<artifactId>kumo-core</artifactId>
	<name>kumo-core</name>
	<version>0.0.15-SNAPSHOT</version>
	<packaging>pom</packaging>
	
	<properties>
		<!-- https://maven.apache.org/maven-ci-friendly.html -->
		<revision>0.0.15-SNAPSHOT</revision>		
		
		<java.version>1.8</java.version>
		<maven.compiler.source>1.8</maven.compiler.source>
        <maven.compiler.target>1.8</maven.compiler.target>
		<project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
		<project.reporting.outputEncoding>UTF-8</project.reporting.outputEncoding>
		
		<amazon-sns-java-extended-client-lib.version>1.0.0</amazon-sns-java-extended-client-lib.version>
		<amazon-sqs-java-extended-client-lib.version>1.2.0</amazon-sqs-java-extended-client-lib.version>
		<apache.cxf.version>3.4.2</apache.cxf.version>
		<apache.poi.version>4.0.1</apache.poi.version>
		<aws-java-sdk.version>1.11.957</aws-java-sdk.version>
		<belike-core.version>0.0.165</belike-core.version>
		<!-- <belike-core.version>[0.0,)</belike-core.version> -->
		<cache-api.version>1.1.1</cache-api.version>
		<com.sun.xml.version>2.3.3</com.sun.xml.version>
		<commons-lang3.version>3.11</commons-lang3.version>
		<commons-net.version>3.7.2</commons-net.version>
		<dozer.version>6.5.0</dozer.version>
		<ehcache3.version>3.9.0</ehcache3.version>
		<freemarker.version>2.3.30</freemarker.version>
		<hibernate.version>5.4.27.Final</hibernate.version>
		<hibernate-search.version>5.11.8.Final</hibernate-search.version>
		<jackson.version>2.11.3</jackson.version>
		<!-- jakarta.jakartaee:9.0.0 cambia los nombres de los packages de javax.* a jakarta.* -->
		<!-- <jakarta.jakartaee.version>9.0.0</jakarta.jakartaee.version>
		<javax.annotation.version>1.3.2</javax.annotation.version>
		<javax.ws.rs.version>2.1.1</javax.ws.rs.version> -->
		<jakarta.jakartaee.version>8.0.0</jakarta.jakartaee.version>
		<jakarta.persistence-api.version>2.2.3</jakarta.persistence-api.version>
		<jakarta.xml.bind-api.version>2.3.3</jakarta.xml.bind-api.version>
		<javax.cache.version>1.1.1</javax.cache.version>
		<jaxb-java-time-adapters.version>1.1.3</jaxb-java-time-adapters.version>
		<glassfish.jersey.version>2.33</glassfish.jersey.version>
		<lombok.version>1.18.16</lombok.version>
		<mapstruct.version>1.4.1.Final</mapstruct.version>
		<mbknor-jackson-jsonschema.version>1.0.34</mbknor-jackson-jsonschema.version>
		<opencsv.version>5.3</opencsv.version>
		<postgresql.version>42.2.18</postgresql.version>
		<querydsl.version>4.4.0</querydsl.version>
		<!-- Esta version del driver JDBC de RedShift no excepciona por problemas de concurrencia -->
		<!-- <redshift.version>1.2.10.1009</redshift.version> -->
		<redshift.version>1.2.45.1069</redshift.version>
		<reflections.version>0.9.12</reflections.version>
		<slf4j.version>1.7.30</slf4j.version>
		<spring-boot.version>2.3.9.RELEASE</spring-boot.version>
		<spring-batch.version>4.2.5.RELEASE</spring-batch.version>
		<spring-data.version>Neumann-SR7</spring-data.version>
		<spring-cloud.version>Hoxton.SR10</spring-cloud.version>
		<spring-framework.version>5.2.13.RELEASE</spring-framework.version>
		<spring-security.version>5.3.8.RELEASE</spring-security.version>
		<threetenbp.version>1.5.0</threetenbp.version>
		
		<!-- TESTING -->
		<junit-platform.version>1.7.0</junit-platform.version>
		<junit-jupiter.version>5.7.0</junit-jupiter.version>
		
		<wagon-git.version>0.3.0</wagon-git.version>
		
		<!-- PLUGINS -->
		<apt-maven-plugin.version>1.1.3</apt-maven-plugin.version>
		<build-helper-plugin.version>3.2.0</build-helper-plugin.version>
		<ci-friendly-flatten-maven-plugin.version>1.0.6</ci-friendly-flatten-maven-plugin.version>
		<flatten-maven-plugin.version>1.2.5</flatten-maven-plugin.version>
		<flyway-maven-plugin.version>7.5.2</flyway-maven-plugin.version>
		<hibernate54-ddl-maven-plugin.version>2.3.0</hibernate54-ddl-maven-plugin.version>
		<jaxb2-basics.version>1.11.1</jaxb2-basics.version>
		<jaxb2-basics-annotate.version>1.1.0</jaxb2-basics-annotate.version>
		<jaxb-fluent-api.version>2.1.8</jaxb-fluent-api.version>
		<jaxb2-namespace-prefix.version>1.3</jaxb2-namespace-prefix.version>
		<jaxb-xew.version>1.11</jaxb-xew.version>
		<jaxb2-maven-plugin.version>2.5.0</jaxb2-maven-plugin.version>
		<jaxws-maven-plugin.version>2.3.1-b20150201.1248</jaxws-maven-plugin.version>
		<jaxws-plugin.version>2.6</jaxws-plugin.version>
		<maven-enforcer-plugin.version>3.0.0-M3</maven-enforcer-plugin.version>
		<maven-jaxb2-plugin.version>0.14.0</maven-jaxb2-plugin.version>
		<maven-dependency-plugin.version>3.1.2</maven-dependency-plugin.version>
		<maven-deploy-plugin.version>3.0.0-M1</maven-deploy-plugin.version>
		<!--  <maven-deploy-plugin.version>2.8.2</maven-deploy-plugin.version> -->
		<maven-release-plugin.version>3.0.0-M1</maven-release-plugin.version>
		<maven-site-plugin.version>3.9.1</maven-site-plugin.version>
		<properties-maven-plugin.version>1.0.0</properties-maven-plugin.version>
		
		<!-- TEST PLUGINS -->
		<maven-surefire-plugin.version>3.0.0-M5</maven-surefire-plugin.version>
		<!-- <maven-surefire-plugin.version>2.22.2</maven-surefire-plugin.version> -->
		<maven-failsafe-plugin.version>3.0.0-M5</maven-failsafe-plugin.version>
		<!-- <maven-failsafe-plugin.version>2.22.2</maven-failsafe-plugin.version> -->
		
		<!-- REPORTING PLUGINS -->
		<maven-checkstyle-plugin.version>3.1.1</maven-checkstyle-plugin.version>
		<maven-javadoc-plugin.version>3.2.0</maven-javadoc-plugin.version>
		<maven-jxr-plugin.version>3.0.0</maven-jxr-plugin.version>
		<maven-pmd-plugin.version>3.13.0</maven-pmd-plugin.version>
		<maven-project-info-reports-plugin.version>3.1.1</maven-project-info-reports-plugin.version>
		<maven-site-plugin.version>3.9.1</maven-site-plugin.version>
		<maven-surefire-report-plugin.version>2.22.2</maven-surefire-report-plugin.version>
		<pitest-maven-plugin.version>1.5.2</pitest-maven-plugin.version>
		<spotbugs-maven-plugin.version>4.1.4</spotbugs-maven-plugin.version>
		<taglist-maven-plugin.version>2.4</taglist-maven-plugin.version>
		
	</properties>
	
	<modules>
		<module>kumo-core-commons</module>
		<module>kumo-core-dao</module>
		<module>kumo-core-dao-bi</module>
		<module>kumo-core-mapper</module>
		<module>kumo-core-vo</module>
		<module>kumo-core-vo-bi</module>
		<module>kumo-core-vo-sap</module>
		<module>kumo-core-vo-sync</module>
		<module>kumo-core-tracer</module>
		<module>kumo-core-util</module>
		<module>kumo-core-services-bi</module>
		<module>kumo-core-services</module>
	</modules>
	
	<dependencies>
		<!-- LOMBOK -->
		<dependency>
			<groupId>org.projectlombok</groupId>
			<artifactId>lombok</artifactId>
			<version>${lombok.version}</version>
			<scope>provided</scope>
		</dependency>
		
		<!-- TEST JUNIT 5-->
		<dependency>
            <groupId>org.junit.platform</groupId>
            <artifactId>junit-platform-engine</artifactId>
            <version>${junit-platform.version}</version>
            <scope>test</scope>
        </dependency>
		<dependency>
            <groupId>org.junit.platform</groupId>
            <artifactId>junit-platform-runner</artifactId>
            <version>${junit-platform.version}</version>
            <scope>test</scope>
            <exclusions>
            	<!-- exclude junit 4 -->
                <exclusion>
                    <groupId>junit</groupId>
                    <artifactId>junit</artifactId>
                </exclusion>
            </exclusions>
        </dependency>
		<dependency>
		    <groupId>org.junit.jupiter</groupId>
		    <artifactId>junit-jupiter-engine</artifactId>
		    <version>${junit-jupiter.version}</version>
		    <scope>test</scope>
		</dependency>
		 <dependency>
            <groupId>org.junit.jupiter</groupId>
            <artifactId>junit-jupiter-api</artifactId>
		    <version>${junit-jupiter.version}</version>
            <scope>test</scope>
        </dependency>
        <dependency>
            <groupId>org.junit.jupiter</groupId>
            <artifactId>junit-jupiter-params</artifactId>
		    <version>${junit-jupiter.version}</version>
            <scope>test</scope>
        </dependency>
	</dependencies>
	
	<dependencyManagement>
		<dependencies>
			<!-- SPRING FRAMEWORK -->
			<dependency>
				<groupId>org.springframework</groupId>
				<artifactId>spring-framework-bom</artifactId>
				<version>${spring-framework.version}</version>
				<scope>import</scope>
				<type>pom</type>
			</dependency>
			
			<!-- SPRING SECURITY -->
			<dependency>
				<groupId>org.springframework.security</groupId>
				<artifactId>spring-security-bom</artifactId>
	            <version>${spring-security.version}</version>
	            <scope>import</scope>
	            <type>pom</type>
	        </dependency>
	        
	        <!-- SPRING DATA -->
	        <dependency>
				<groupId>org.springframework.data</groupId>
				<artifactId>spring-data-releasetrain</artifactId>
				<version>${spring-data.version}</version>
				<scope>import</scope>
				<type>pom</type>
			</dependency>
			
			<!-- SPRING CLOUD -->
			<dependency>
				<groupId>org.springframework.cloud</groupId>
				<artifactId>spring-cloud-dependencies</artifactId>
				<version>${spring-cloud.version}</version>
				<scope>import</scope>
				<type>pom</type>
			</dependency>
			
			<!-- AMAZON AWS SDK -->
			<dependency>
			    <groupId>com.amazonaws</groupId>
			    <artifactId>aws-java-sdk-bom</artifactId>
			    <version>${aws-java-sdk.version}</version>
				<scope>import</scope>
			    <type>pom</type>
			</dependency>
			
			<!-- JACKSON -->
			<dependency>
				<groupId>com.fasterxml.jackson</groupId>
				<artifactId>jackson-bom</artifactId>
				<version>${jackson.version}</version>
				<scope>import</scope>
				<type>pom</type>
			</dependency>
			
			<!-- GLASSFISH JERSEY -->
			<dependency>
				<groupId>org.glassfish.jersey</groupId>
				<artifactId>jersey-bom</artifactId>
				<version>${glassfish.jersey.version}</version>
				<scope>import</scope>
				<type>pom</type>
			</dependency>
			
			<!-- JAKARTA EE API -->
			<dependency>
				<groupId>jakarta.platform</groupId>
				<artifactId>jakarta.jakartaee-bom</artifactId>
				<version>${jakarta.jakartaee.version}</version>
				<scope>import</scope>
				<type>pom</type>
				<exclusions>
					<!-- <exclusion>
						<groupId>javax.ws.rs</groupId>
						<artifactId>javax.ws.rs-api</artifactId>
        			</exclusion> -->
        			<!-- jakarta.jakartaee-bom:8.0.0 tiene mal definida la versión de esta dependencia -->
        			<exclusion>
        				<groupId>jakarta.persistence</groupId>
    					<artifactId>jakarta.persistence-api</artifactId>
        			</exclusion>
				</exclusions>
			</dependency>
			<dependency>
    			<groupId>jakarta.persistence</groupId>
    			<artifactId>jakarta.persistence-api</artifactId>
    			<!-- jakarta.jakartaee-bom:8.0.0 tiene mal definida la versión de esta dependencia -->
    			<version>${jakarta.persistence-api.version}</version>
			</dependency>
						
			<!-- JUNIT 5 -->
			<dependency>
			    <groupId>org.junit</groupId>
			    <artifactId>junit-bom</artifactId>
			    <version>${junit-jupiter.version}</version>
			    <type>pom</type>
			</dependency>
			
			<!-- MAPSTRUCT -->
			<dependency>
				<groupId>org.mapstruct</groupId>
	        	<artifactId>mapstruct</artifactId>
				<version>${mapstruct.version}</version>
			</dependency>
			
			<!-- BELIKE-CORE-DATA -->
			<dependency>
				<groupId>com.belike.belike-core</groupId>
				<artifactId>belike-core-data</artifactId>
				<version>${belike-core.version}</version>
			</dependency>
			<dependency>
				<groupId>com.belike.belike-core</groupId>
				<artifactId>belike-core-data-querydsl</artifactId>
				<version>${belike-core.version}</version>
			</dependency>
			<!-- BELIKE-CORE-EMAIL -->
			<dependency>
				<groupId>com.belike.belike-core</groupId>
				<artifactId>belike-core-email</artifactId>
				<version>${belike-core.version}</version>
			</dependency>
			<!-- BELIKE-CORE-ERROR -->
			<dependency>
				<groupId>com.belike.belike-core</groupId>
				<artifactId>belike-core-error</artifactId>
				<version>${belike-core.version}</version>
			</dependency>
			<!-- BELIKE-CORE-MAPPER -->
			<dependency>
				<groupId>com.belike.belike-core</groupId>
				<artifactId>belike-core-mapper</artifactId>
				<version>${belike-core.version}</version>
			</dependency>
			<!-- BELIKE-CORE-REST-CLIENT -->
			<dependency>
				<groupId>com.belike.belike-core</groupId>
				<artifactId>belike-core-rest-client</artifactId>
				<version>${belike-core.version}</version>
			</dependency>
			<!-- BELIKE-CORE-UTIL -->
			<dependency>
				<groupId>com.belike.belike-core</groupId>
				<artifactId>belike-core-util</artifactId>
				<version>${belike-core.version}</version>
			</dependency>
			<!-- BELIKE-CORE-WEBSOCKET -->
			<dependency>
				<groupId>com.belike.belike-core</groupId>
				<artifactId>belike-core-websocket</artifactId>
				<version>${belike-core.version}</version>
			</dependency>
		</dependencies>
	</dependencyManagement>
		
	<build>
		<extensions>
			<extension>
				<groupId>ar.com.synergian</groupId>
				<artifactId>wagon-git</artifactId>
				<version>${wagon-git.version}</version>
			</extension>
		</extensions>
		
		<plugins>
			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-enforcer-plugin</artifactId>
				<version>${maven-enforcer-plugin.version}</version>
				<executions>
					<execution>
						<id>enforce-maven</id>
						<goals>
							<goal>enforce</goal>
						</goals>
						<configuration>
							<rules>
								<requireMavenVersion>
									<version>[3.6,)</version>
								</requireMavenVersion>
							</rules>
						</configuration>
					</execution>
				</executions>
			</plugin>
			
			<!-- <plugin>
				<groupId>org.codehaus.mojo</groupId>
				<artifactId>flatten-maven-plugin</artifactId>
				<version>${flatten-maven-plugin.version}</version>
				<configuration>
					<updatePomFile>true</updatePomFile>
					<flattenMode>resolveCiFriendliesOnly</flattenMode>
				</configuration>
				<executions>
					<execution>
						<id>flatten</id>
						<phase>process-resources</phase>
						<goals>
							<goal>flatten</goal>
						</goals>
					</execution>
					<execution>
						<id>flatten.clean</id>
						<phase>clean</phase>
						<goals>
							<goal>clean</goal>
						</goals>
					</execution>
				</executions>
			</plugin> -->
			
			<!-- https://github.com/outbrain/ci-friendly-flatten-maven-plugin -->
			<plugin>
				<groupId>com.outbrain.swinfra</groupId>
				<artifactId>ci-friendly-flatten-maven-plugin</artifactId>
				<version>${ci-friendly-flatten-maven-plugin.version}</version>
				<executions>
					<execution>
						<id>flatten.clean</id>
			            <phase>clean</phase>
			            <goals>
			            	<goal>clean</goal>
			            </goals>
			        </execution>
			        <execution>
			          	<id>flatten</id>
						<phase>process-resources</phase>
						<goals>
							<goal>flatten</goal>
						</goals>
					</execution>
				</executions>
			</plugin>
			
			<plugin>
	            <groupId>org.springframework.boot</groupId>
	            <artifactId>spring-boot-maven-plugin</artifactId>
	            <version>${spring-boot.version}</version>
	        </plugin>
			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-site-plugin</artifactId>
			</plugin>
		</plugins>
		
		<pluginManagement>
			<plugins>
				<!-- HIBERNATE 5.4.x DDL GENERATOR -->
				<plugin>
					<groupId>de.jpdigital</groupId>
			   		<artifactId>hibernate54-ddl-maven-plugin</artifactId>
			   		<version>${hibernate54-ddl-maven-plugin.version}</version>
			   		<dependencies>
			   			<dependency>
						    <groupId>org.threeten</groupId>
						    <artifactId>threetenbp</artifactId>
						    <version>${threetenbp.version}</version>
						</dependency>
			   		</dependencies>
				</plugin>
				
				<!-- FLYWAY -->
				<plugin>
					<groupId>org.flywaydb</groupId>
					<artifactId>flyway-maven-plugin</artifactId>
					<version>${flyway-maven-plugin.version}</version>
					<dependencies>
						<!-- JDBC -->
						<dependency>
							<groupId>org.postgresql</groupId>
							<artifactId>postgresql</artifactId>
							<version>${postgresql.version}</version>
						</dependency>
					</dependencies>
					<executions>
						<execution>
							<id>flyway</id>
						</execution>
					</executions>
				</plugin>
				
				<!-- QUERY DSL GENERATOR -->
				<plugin>
					<groupId>com.mysema.maven</groupId>
					<artifactId>apt-maven-plugin</artifactId>
					<version>${apt-maven-plugin.version}</version>
					<dependencies>
						<dependency>
							<groupId>com.querydsl</groupId>
							<artifactId>querydsl-apt</artifactId>
							<version>${querydsl.version}</version>
						</dependency>
						<dependency>
							<groupId>com.querydsl</groupId>
							<artifactId>querydsl-jpa</artifactId>
							<classifier>apt</classifier>
							<version>${querydsl.version}</version>
						</dependency>
						<!-- Si se utiliza jakarta.jakartaee:9.0.0 -->
						<!-- <dependency>
							<groupId>javax.annotation</groupId>
							<artifactId>javax.annotation-api</artifactId>
							<version>${javax.annotation.version}</version>
						</dependency> -->
					</dependencies>
				</plugin>
				
				<!-- BUILD HELPER -->
	            <plugin>
				    <groupId>org.codehaus.mojo</groupId>
				    <artifactId>build-helper-maven-plugin</artifactId>
				    <version>${build-helper-plugin.version}</version>
				</plugin>
				
				<!-- MAVEN DEPLOY PLUGIN -->
				<plugin>
				    <groupId>org.apache.maven.plugins</groupId>
				    <artifactId>maven-deploy-plugin</artifactId>
				    <version>${maven-deploy-plugin.version}</version>
				</plugin>
				
				<!-- MAVEN RELEASE PLUGIN -->
				<plugin>
			        <groupId>org.apache.maven.plugins</groupId>
			        <artifactId>maven-release-plugin</artifactId>
			        <version>${maven-release-plugin.version}</version>
			      </plugin>
			      
			      <!-- MAVEN SUREFIRE PLUGIN -->
				<plugin>
				    <groupId>org.apache.maven.plugins</groupId>
				    <artifactId>maven-surefire-plugin</artifactId>
				    <version>${maven-surefire-plugin.version}</version>
				</plugin>
				
				<!-- MAVEN FAILSAFE PLUGIN -->
				<plugin>
				    <groupId>org.apache.maven.plugins</groupId>
				    <artifactId>maven-failsafe-plugin</artifactId>
				    <version>${maven-failsafe-plugin.version}</version>
				</plugin>
				
				<!-- MAVEN SITE PLUGIN -->
				<plugin>
					<groupId>org.apache.maven.plugins</groupId>
					<artifactId>maven-site-plugin</artifactId>
					<version>${maven-site-plugin.version}</version>
				</plugin>
				
				<!-- MAVEN DEPENDENCY -->
				<plugin>
			    	<groupId>org.apache.maven.plugins</groupId>
				    <artifactId>maven-dependency-plugin</artifactId>
				    <version>${maven-dependency-plugin.version}</version>
				    <configuration>
				    	<ignoredUnusedDeclaredDependencies>
		                	<ignoredUnusedDeclaredDependency>org.projectlombok:lombok</ignoredUnusedDeclaredDependency>
		                </ignoredUnusedDeclaredDependencies>
		            </configuration>
				</plugin>
				
				<!--This plugin's configuration is used to store Eclipse m2e settings 
				only. It has no influence on the Maven build itself. -->
				<!-- <plugin>
					<groupId>org.eclipse.m2e</groupId>
					<artifactId>lifecycle-mapping</artifactId>
					<version>1.0.0</version>
					<configuration>
						<lifecycleMappingMetadata>
							<pluginExecutions>
								<pluginExecution>
									<pluginExecutionFilter>
										<groupId>org.codehaus.mojo</groupId>
										<artifactId>flatten-maven-plugin</artifactId>
										<versionRange>[1.2.,)</versionRange>
										<goals>
											<goal>flatten</goal>
											<goal>clean</goal>
										</goals>
									</pluginExecutionFilter>
									<action>
										<execute />
									</action>
								</pluginExecution>
							</pluginExecutions>
						</lifecycleMappingMetadata>
					</configuration>
				</plugin> -->
			</plugins>
		</pluginManagement>
	</build>
	
	<reporting>
		<plugins>
			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-project-info-reports-plugin</artifactId>
				<version>${maven-project-info-reports-plugin.version}</version>
				<configuration>
					<dependencyDetailsEnabled>false</dependencyDetailsEnabled>
					<dependencyLocationsEnabled>false</dependencyLocationsEnabled>
				</configuration>
			</plugin>
			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-surefire-report-plugin</artifactId>
				<version>${maven-surefire-report-plugin.version}</version>
				<configuration>
					<outputDirectory>${project.reporting.outputDirectory}/testresults</outputDirectory>
				</configuration>
			</plugin>
			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-javadoc-plugin</artifactId>
				<version>${maven-javadoc-plugin.version}</version>
				<configuration>
                    <additionalparam>-Xdoclint:none</additionalparam>
                    <aggregate>true</aggregate>
                    <failOnError>false</failOnError>
                </configuration>
			</plugin>
			<plugin>
	            <groupId>org.apache.maven.plugins</groupId>
	            <artifactId>maven-jxr-plugin</artifactId>
	            <version>${maven-jxr-plugin.version}</version>
	        </plugin>
			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-checkstyle-plugin</artifactId>
				<version>${maven-checkstyle-plugin.version}</version>
			</plugin>
			<plugin>
				<groupId>com.github.spotbugs</groupId>
				<artifactId>spotbugs-maven-plugin</artifactId>
				<version>${spotbugs-maven-plugin.version}</version>
			</plugin>
	        <plugin>
       	 		<groupId>org.apache.maven.plugins</groupId>
        		<artifactId>maven-pmd-plugin</artifactId>
        		<version>${maven-pmd-plugin.version}</version>
      		</plugin>
			<plugin>
				<groupId>org.codehaus.mojo</groupId>
				<artifactId>taglist-maven-plugin</artifactId>
				<version>${taglist-maven-plugin.version}</version>
				<!-- <configuration>
					<aggregate>true</aggregate>
				</configuration> -->
			</plugin>
			<plugin>
                <groupId>org.owasp</groupId>
                <artifactId>dependency-check-maven</artifactId>
                <version>${dependency-checker-maven.version}</version>
                <reportSets>
                    <reportSet>
                        <reports>
                            <report>aggregate</report>
                        </reports>
                    </reportSet>
                </reportSets>
            </plugin>
		</plugins>
	</reporting>
	
	<repositories>
		<repository>
			<id>central</id>
			<name>Maven Central Repository</name>
			<url>https://repo.maven.apache.org/maven2</url>
		</repository>
		<repository>
			<id>beLike</id>
			<name>beLike repo</name>
			<url>https://api.bitbucket.org/2.0/repositories/belike/belike-maven-repository.git/src/releases</url>
		</repository>
		<repository>
			<id>kumo-releases</id>
			<name>kumo releases repo</name>
			<url>https://api.bitbucket.org/2.0/repositories/seguradev/kumo-maven-repository.git/src/releases</url>
		</repository>
		<!-- <repository>
			<id>jr-ce</id>
			<name>JasperReports CE</name>
			<url>http://jaspersoft.artifactoryonline.com/jaspersoft/jr-ce-releases</url>
		</repository> -->
		<repository>
			<id>activeeon</id>
			<name>Activeeon</name>
			<url>http://repository.activeeon.com/content/repositories/releases/</url>
		</repository>
		<repository>
			<id>redshift</id>
			<url>http://redshift-maven-repository.s3-website-us-east-1.amazonaws.com/release</url>
		</repository>
		<repository>
	        <id>sonatype-nexus</id>
	        <name>Sonatype Nexus</name>
	        <url>https://oss.sonatype.org/content/repositories/</url>
	    </repository>
	    
	    <!-- SNAPSHOTS -->
		<repository>
			<id>kumo-snapshots</id>
			<name>kumo snapshots repo</name>
			<url>https://api.bitbucket.org/2.0/repositories/seguradev/kumo-maven-repository.git/src/snapshots</url>
			<releases>
				<enabled>false</enabled>
			</releases>
			<snapshots>
				<enabled>true</enabled>
			</snapshots>
		</repository>
	</repositories>
	
	<pluginRepositories>
		<pluginRepository>
			<id>spring-releases</id>
			<name>Spring GA Repository</name>
			<url>https://repo.spring.io/plugins-release/</url>
		</pluginRepository>
		<pluginRepository>
			<id>jr-ce</id>
			<name>JasperReports CE</name>
			<url>http://jaspersoft.artifactoryonline.com/jaspersoft/jr-ce-releases</url>
		</pluginRepository>
		<pluginRepository>
			<id>synergian-repo</id>
			<url>https://raw.github.com/synergian/wagon-git/releases</url>
		</pluginRepository>
	</pluginRepositories>
	
	<distributionManagement>
		<repository>
			<id>kumo-releases</id>
			<name>kumo releases repo</name>
			<url>git:releases://git@bitbucket.org:seguradev/kumo-maven-repository.git</url>
		</repository>
		<snapshotRepository>
			<id>kumo-snapshots</id>
			<name>kumo snapshots repo</name>
			<url>git:snapshots://git@bitbucket.org:seguradev/kumo-maven-repository.git</url>
		</snapshotRepository>
		<site>
			<id>${project.artifactId}-site</id>
			<url>${project.baseUri}</url>
		</site>
	</distributionManagement>
	
	<scm>
		<connection>scm:git:ssh://git@bitbucket.org/seguradev/kumo-core.git</connection>
		<developerConnection>scm:git:ssh://git@bitbucket.org/seguradev/kumo-core.git</developerConnection>
		<url>https://bitbucket.org/seguradev/kumo-core</url>
		<tag>HEAD</tag>
	</scm>
	
	<ciManagement>
		<system>jenkins</system>
		<url>https://jenkins.fsegura.com/</url>
	</ciManagement>
	
	<organization>
		<name>Grupo Segura</name>
		<url>https://www.fsegura.com/</url>
	</organization>
	
	<developers>
		<developer>
			<id>rialonso</id>
			<name>Ricardo Alonso</name>
			<email>rialonso@fsegura.com</email>
			<organization>Grupo Segura</organization>
		</developer>
	</developers>
	
</project>