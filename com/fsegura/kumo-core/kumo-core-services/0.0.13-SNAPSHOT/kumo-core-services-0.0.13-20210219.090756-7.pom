<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 https://maven.apache.org/xsd/maven-4.0.0.xsd">
	<modelVersion>4.0.0</modelVersion>
	
	<parent>
		<groupId>com.fsegura.kumo-core</groupId>
		<artifactId>kumo-core</artifactId>
		<version>0.0.13-SNAPSHOT</version>
	</parent>
	
	<artifactId>kumo-core-services</artifactId>
	<name>kumo-core-services</name>
	<packaging>jar</packaging>
	
	<dependencies>
		<!-- SPRING FRAMEWORK -->
		<dependency>
			<groupId>org.springframework</groupId>
			<artifactId>spring-context</artifactId>
		</dependency>
		<dependency>
			<groupId>org.springframework</groupId>
			<artifactId>spring-aspects</artifactId>
		</dependency>
		<!-- SPRING BATCH -->
		<!-- <dependency>
			<groupId>org.springframework.batch</groupId>
			<artifactId>spring-batch-core</artifactId>
			<version>${spring-batch.version}</version>
		</dependency> -->
		
		<!-- FREEMARKER -->
		<dependency>
			<groupId>org.freemarker</groupId>
			<artifactId>freemarker</artifactId>
			<version>${freemarker.version}</version>
		</dependency>
		
		<!-- KUMO-CORE-COMMONS -->
		<dependency>
			<groupId>com.fsegura.kumo-core</groupId>
			<artifactId>kumo-core-commons</artifactId>
			<version>${project.version}</version>
		</dependency>
		<!-- KUMO-CORE-MAPPER -->
		<dependency>
			<groupId>com.fsegura.kumo-core</groupId>
			<artifactId>kumo-core-mapper</artifactId>
			<version>${project.version}</version>
		</dependency>
		<!-- KUMO-CORE-UTIL -->
		<dependency>
			<groupId>com.fsegura.kumo-core</groupId>
			<artifactId>kumo-core-util</artifactId>
			<version>${project.version}</version>
		</dependency>
		<!-- KUMO-CORE-VO -->
		<dependency>
			<groupId>com.fsegura.kumo-core</groupId>
			<artifactId>kumo-core-vo</artifactId>
			<version>${project.version}</version>
			<exclusions>
				<exclusion>
					<!-- KUMO-DAO -->
					<groupId>com.fsegura.kumo-core</groupId>
					<artifactId>kumo-core-dao</artifactId>
				</exclusion>
			</exclusions>
		</dependency>
		<!-- KUMO-CORE-DAO -->
		<dependency>
			<groupId>com.fsegura.kumo-core</groupId>
			<artifactId>kumo-core-dao</artifactId>
			<version>${project.version}</version>
		</dependency>
		<!-- KUMO-CORE-DAO-BI -->
		<dependency>
			<groupId>com.fsegura.kumo-core</groupId>
			<artifactId>kumo-core-dao-bi</artifactId>
			<version>${project.version}</version>
		</dependency>
		<!-- KUMO-CORE-VO-BI -->
		<dependency>
			<groupId>com.fsegura.kumo-core</groupId>
			<artifactId>kumo-core-vo-bi</artifactId>
			<version>${project.version}</version>
		</dependency>
		
		<!-- BELIKE-CORE-EMAIL -->
		<dependency>
			<groupId>com.belike.belike-core</groupId>
			<artifactId>belike-core-email</artifactId>
		</dependency>
		<!-- BELIKE-CORE-MAPPER -->
		<dependency>
			<groupId>com.belike.belike-core</groupId>
			<artifactId>belike-core-mapper</artifactId>
		</dependency>
		<!-- BELIKE-CORE-REST-CLIENT -->
		<dependency>
			<groupId>com.belike.belike-core</groupId>
			<artifactId>belike-core-rest-client</artifactId>
		</dependency>
		<!-- BELIKE-CORE-WEBSOCKET -->
		<dependency>
			<groupId>com.belike.belike-core</groupId>
			<artifactId>belike-core-websocket</artifactId>
		</dependency>
		
		<!-- COMMONS LANG -->
		<dependency>
			<groupId>org.apache.commons</groupId>
			<artifactId>commons-lang3</artifactId>
			<version>${commons-lang3.version}</version>
		</dependency>
				
		<!-- OpenCSV -->
		<dependency>
    		<groupId>com.opencsv</groupId>
    		<artifactId>opencsv</artifactId>
    		<version>${opencsv.version}</version>
		</dependency>
				
		<!-- APACHE POI EXCEL GENERATOR V4 -->
		<dependency>
			<groupId>org.apache.poi</groupId>
    		<artifactId>poi-ooxml</artifactId>
    		<version>${apache.poi.version}</version>
		</dependency>
		
		<!-- JAKARTA WS.RS -->
        <dependency>
        	<groupId>jakarta.ws.rs</groupId>
    		<artifactId>jakarta.ws.rs-api</artifactId>
    		<scope>provided</scope>
		</dependency>
		
		<!-- jersey-media-multipart -->
		<dependency>
    		<groupId>org.glassfish.jersey.media</groupId>
    		<artifactId>jersey-media-multipart</artifactId>
		</dependency>		

		<!-- AMAZON AWS SES -->
		<dependency>
			<groupId>com.amazonaws</groupId>
			<artifactId>aws-java-sdk-ses</artifactId>
		</dependency>

		<!-- MAIL -->
		<dependency>
			<groupId>com.sun.mail</groupId>
			<artifactId>jakarta.mail</artifactId>
			<!-- <version>${jakarta.mail.version}</version> -->
		</dependency>
	</dependencies>
	
	<build>
		<plugins>
			<!-- <plugin>
				<groupId>org.codehaus.mojo</groupId>
				<artifactId>properties-maven-plugin</artifactId>
				<version>${properties-maven-plugin.version}</version>
				<executions>
					<execution>
						<phase>initialize</phase>
						<goals>
							<goal>read-project-properties</goal>
						</goals>
						<configuration>
							<files>
								<file>${basedir}/src/main/resources/core.properties</file>
							</files>
						</configuration>
					</execution>
				</executions>
			</plugin> -->
			<plugin>
				<groupId>org.flywaydb</groupId>
				<artifactId>flyway-maven-plugin</artifactId>
				<configuration>
					<skip>true</skip>
				</configuration>
			</plugin>
			
			
			<!-- <plugin>
	            <groupId>org.codehaus.mojo</groupId>
	            <artifactId>jaxb2-maven-plugin</artifactId>
	            <executions>
	            </executions>
	            <configuration>
	            	<outputDirectory>${project.build.directory}/generated-resources/schemagen</outputDirectory>
	            	<sources>
	            		<source>src/main/java/com/belike/fsegura/kumo/core/vo/sync/SyncApiGatewayRequestVO.java</source>
	            		<source>src/main/java/com/belike/fsegura/kumo/core/vo/sync/external/base/VOCustomer</source>
	            	</sources>
	            	<schemaSourceExcludeFilters>
	            		<noJaxbIndex implementation="org.codehaus.mojo.jaxb2.shared.filters.pattern.PatternFileFilter">
	            			<patterns>
	                            <pattern>src/main/java/com/belike/fsegura/kumo/core/vo/EmailCreationVO.java</pattern>
	                            <pattern>src/main/java/com/belike/fsegura/kumo/core/vo/TranslationVO.java</pattern>
	                            <pattern>src/main/java/com/belike/fsegura/kumo/core/vo/TranslationLanguageVO.java</pattern>
	                        </patterns>
	            		</noJaxbIndex>
	            	</schemaSourceExcludeFilters>
	            	<transformSchemas>
	            		<transformSchema>
	            			<uri>http://fsegura.com/core/vo/sync</uri>
	            			<toPrefix>sync_vo</toPrefix>
	            			<toFile>sync_vo_schema.xsd</toFile>
	            		</transformSchema>
	            		<transformSchema>
	            			<uri>http://fsegura.com/core/vo/sync/external/base</uri>
	            			<toPrefix>sync_vo_external_base</toPrefix>
	            			<toFile>sync_vo_external_base_schema.xsd</toFile>
	            		</transformSchema>
	            	</transformSchemas>
	            </configuration>
	        </plugin> -->
		</plugins>
	</build>
</project>