 <project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 https://maven.apache.org/xsd/maven-4.0.0.xsd">
	<modelVersion>4.0.0</modelVersion>
	
	<parent>
		<groupId>com.fsegura.kumo-core</groupId>
		<artifactId>kumo-core</artifactId>
		<version>0.0.47-SNAPSHOT</version>
	</parent>
	
	<artifactId>kumo-core-services</artifactId>
	<name>kumo-core-services</name>
	<packaging>jar</packaging>
	
	<dependencies>
		<!-- SPRING FRAMEWORK -->
		<dependency>
			<groupId>org.springframework</groupId>
			<artifactId>spring-context</artifactId>
		</dependency>
		<dependency>
			<groupId>org.springframework</groupId>
			<artifactId>spring-aspects</artifactId>
		</dependency>
		<!-- SPRING BATCH -->
		<!-- <dependency>
			<groupId>org.springframework.batch</groupId>
			<artifactId>spring-batch-core</artifactId>
			<version>${spring-batch.version}</version>
		</dependency> -->
		
		<!-- FREEMARKER -->
		<dependency>
			<groupId>org.freemarker</groupId>
			<artifactId>freemarker</artifactId>
		</dependency>
		
		<!-- KUMO-CORE-COMMONS -->
		<dependency>
			<groupId>com.fsegura.kumo-core</groupId>
			<artifactId>kumo-core-dao-commons</artifactId>
			<version>${project.version}</version>
		</dependency>
		<dependency>
			<groupId>com.fsegura.kumo-core</groupId>
			<artifactId>kumo-core-error</artifactId>
			<version>${project.version}</version>
		</dependency>
		<dependency>
			<groupId>com.fsegura.kumo-core</groupId>
			<artifactId>kumo-core-file</artifactId>
			<version>${project.version}</version>
		</dependency>
		<!-- KUMO-CORE-MAPPER -->
		<dependency>
			<groupId>com.fsegura.kumo-core</groupId>
			<artifactId>kumo-core-mapper</artifactId>
			<version>${project.version}</version>
		</dependency>
		<!-- KUMO-CORE-UTIL -->
		<dependency>
			<groupId>com.fsegura.kumo-core</groupId>
			<artifactId>kumo-core-util</artifactId>
			<version>${project.version}</version>
		</dependency>
		<!-- KUMO-CORE-VO -->
		<dependency>
			<groupId>com.fsegura.kumo-core</groupId>
			<artifactId>kumo-core-vo</artifactId>
			<version>${project.version}</version>
			<exclusions>
				<exclusion>
					<!-- KUMO-DAO -->
					<groupId>com.fsegura.kumo-core</groupId>
					<artifactId>kumo-core-dao</artifactId>
				</exclusion>
			</exclusions>
		</dependency>
		<!-- KUMO-CORE-DAO -->
		<dependency>
			<groupId>com.fsegura.kumo-core</groupId>
			<artifactId>kumo-core-dao</artifactId>
			<version>${project.version}</version>
		</dependency>
		<!-- KUMO-CORE-DAO-BI -->
		<dependency>
			<groupId>com.fsegura.kumo-core</groupId>
			<artifactId>kumo-core-dao-bi</artifactId>
			<version>${project.version}</version>
		</dependency>
		<!-- KUMO-CORE-VO-BI -->
		<dependency>
			<groupId>com.fsegura.kumo-core</groupId>
			<artifactId>kumo-core-vo-bi</artifactId>
			<version>${project.version}</version>
		</dependency>
		
		<!-- BELIKE-CORE-EMAIL -->
		<dependency>
			<groupId>com.belike.belike-core</groupId>
			<artifactId>belike-core-email</artifactId>
		</dependency>
		<!-- BELIKE-CORE-MAPPER -->
		<dependency>
			<groupId>com.belike.belike-core</groupId>
			<artifactId>belike-core-mapper</artifactId>
		</dependency>
		<!-- BELIKE-CORE-REST-CLIENT -->
		<dependency>
			<groupId>com.belike.belike-core</groupId>
			<artifactId>belike-core-rest-client</artifactId>
		</dependency>
		<!-- BELIKE-CORE-WEBSOCKET -->
		<dependency>
			<groupId>com.belike.belike-core</groupId>
			<artifactId>belike-core-websocket</artifactId>
		</dependency>
		
		<!-- COMMONS LANG -->
		<dependency>
			<groupId>org.apache.commons</groupId>
			<artifactId>commons-lang3</artifactId>
		</dependency>
				
		<!-- OpenCSV -->
		<dependency>
    		<groupId>com.opencsv</groupId>
    		<artifactId>opencsv</artifactId>
    		<version>${opencsv.version}</version>
		</dependency>
				
		<!-- APACHE POI EXCEL GENERATOR V4 -->
		<dependency>
			<groupId>org.apache.poi</groupId>
    		<artifactId>poi-ooxml</artifactId>
    		<version>${apache.poi.version}</version>
		</dependency>
		
		<!-- JAKARTA WS.RS -->
        <dependency>
        	<groupId>jakarta.ws.rs</groupId>
    		<artifactId>jakarta.ws.rs-api</artifactId>
    		<scope>provided</scope>
		</dependency>
		
		<dependency>
			<groupId>jakarta.xml.ws</groupId>
			<artifactId>jakarta.xml.ws-api</artifactId>
		</dependency>
		
		<dependency>
			<groupId>jakarta.jws</groupId>
			<artifactId>jakarta.jws-api</artifactId>
		</dependency>
		
		<!-- jersey-media-multipart -->
		<dependency>
    		<groupId>org.glassfish.jersey.media</groupId>
    		<artifactId>jersey-media-multipart</artifactId>
		</dependency>		

		<!-- AMAZON AWS SES -->
		<dependency>
			<groupId>com.amazonaws</groupId>
			<artifactId>aws-java-sdk-ses</artifactId>
		</dependency>

		<!-- MAIL -->
		<dependency>
			<groupId>com.sun.mail</groupId>
			<artifactId>jakarta.mail</artifactId>
			<!-- <version>${jakarta.mail.version}</version> -->
		</dependency>
	</dependencies>
	
	<build>
		<resources>
			<resource>
				<directory>src/main/resources</directory>
				<filtering>true</filtering>
				<includes>
					<include>**/*.xml</include>
				</includes>
				<!-- <excludes>
					<exclude>**/*.properties</exclude>
				</excludes> -->
			</resource>
		</resources>
		
		<testResources>
			<testResource>
				<directory>src/test/resources</directory>
				<includes>
					<include>**/*.xml</include>
				</includes>
			</testResource>
		</testResources>
		
		<plugins>
			<!-- <plugin>
				<groupId>org.codehaus.mojo</groupId>
				<artifactId>properties-maven-plugin</artifactId>
				<version>${properties-maven-plugin.version}</version>
				<executions>
					<execution>
						<phase>initialize</phase>
						<goals>
							<goal>read-project-properties</goal>
						</goals>
						<configuration>
							<files>
								<file>${basedir}/src/main/resources/core.properties</file>
							</files>
						</configuration>
					</execution>
				</executions>
			</plugin> -->
			<plugin>
				<groupId>org.flywaydb</groupId>
				<artifactId>flyway-maven-plugin</artifactId>
				<configuration>
					<skip>true</skip>
				</configuration>
			</plugin>
			
			<!-- SPOTBUGS MAVEN PLUGIN -->
			<plugin>
				<groupId>com.github.spotbugs</groupId>
				<artifactId>spotbugs-maven-plugin</artifactId>
				<configuration>
					<excludeFilterFile>${basedir}/src/main/resources/spotbugs/spotbugs-exclude.xml</excludeFilterFile>
				</configuration>
			</plugin>
			
		</plugins>
	</build>
</project>