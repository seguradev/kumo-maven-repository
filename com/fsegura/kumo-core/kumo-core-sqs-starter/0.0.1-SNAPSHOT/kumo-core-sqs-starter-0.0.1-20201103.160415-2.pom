<project xmlns="http://maven.apache.org/POM/4.0.0"
	xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
	xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 https://maven.apache.org/xsd/maven-4.0.0.xsd">
	<modelVersion>4.0.0</modelVersion>

	<groupId>com.fsegura.kumo-core</groupId>
	<artifactId>kumo-core-sqs-starter</artifactId>
	<name>kumo-core-sqs-starter</name>
	<version>0.0.1-SNAPSHOT</version>
	<description>AWS SQS Spring Boot Starter</description>

	<properties>
		<maven.compiler.source>1.8</maven.compiler.source>
		<maven.compiler.target>1.8</maven.compiler.target>
		<project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
		<project.reporting.outputEncoding>UTF-8</project.reporting.outputEncoding>

		<aws-java-sdk.version>1.11.885</aws-java-sdk.version>
		<amazon-sqs-java-extended-client-lib.version>1.2.0</amazon-sqs-java-extended-client-lib.version>
		<commons-lang3.version>3.11</commons-lang3.version>
		<kumo-core.version>0.0.1-SNAPSHOT</kumo-core.version>
		<lombok.version>1.18.16</lombok.version>
		<micrometer.version>1.5.5</micrometer.version>
		<resilience4j.version>1.6.1</resilience4j.version>
		<spring-boot.version>2.3.4.RELEASE</spring-boot.version>
		<spring-cloud.version>Hoxton.SR8</spring-cloud.version>


		<!-- TESTING -->
		<junit-platform.version>1.7.0</junit-platform.version>
		<junit-jupiter.version>5.7.0</junit-jupiter.version>

		<!-- PLUGINS -->
		<build-helper-plugin.version>3.2.0</build-helper-plugin.version>
		<maven-dependency-plugin.version>3.1.2</maven-dependency-plugin.version>
		<maven-deploy-plugin.version>3.0.0-M1</maven-deploy-plugin.version>
		<maven-release-plugin.version>3.0.0-M1</maven-release-plugin.version>
		<maven-site-plugin.version>3.9.1</maven-site-plugin.version>
		<wagon-git.version>0.3.0</wagon-git.version>

		<!-- TEST PLUGINS -->
		<maven-surefire-plugin.version>3.0.0-M5</maven-surefire-plugin.version>
		<maven-failsafe-plugin.version>3.0.0-M5</maven-failsafe-plugin.version>

	</properties>

	<dependencies>
		<dependency>
			<groupId>org.springframework.boot</groupId>
			<artifactId>spring-boot-starter</artifactId>
		</dependency>
		
		<dependency>
			<groupId>org.springframework.boot</groupId>
			<artifactId>spring-boot-configuration-processor</artifactId>
			<optional>true</optional>
		</dependency>
		
		<dependency>
			<groupId>io.micrometer</groupId>
			<artifactId>micrometer-core</artifactId>
		</dependency>

		<dependency>
			<groupId>com.amazonaws</groupId>
			<artifactId>amazon-sqs-java-extended-client-lib</artifactId>
			<version>${amazon-sqs-java-extended-client-lib.version}</version>
		</dependency>

		<dependency>
			<groupId>com.amazonaws</groupId>
			<artifactId>aws-java-sdk-core</artifactId>
			<version>${aws-java-sdk.version}</version><!--$NO-MVN-MAN-VER$-->
		</dependency>
		<dependency>
			<groupId>com.amazonaws</groupId>
			<artifactId>aws-java-sdk-sqs</artifactId>
			<version>${aws-java-sdk.version}</version><!--$NO-MVN-MAN-VER$-->
		</dependency>
		<dependency>
			<groupId>com.amazonaws</groupId>
			<artifactId>aws-java-sdk-s3</artifactId>
			<version>${aws-java-sdk.version}</version><!--$NO-MVN-MAN-VER$-->
		</dependency>
		<dependency>
			<groupId>com.amazonaws</groupId>
			<artifactId>aws-java-sdk-kms</artifactId>
			<version>${aws-java-sdk.version}</version><!--$NO-MVN-MAN-VER$-->
		</dependency>

		<dependency>
			<groupId>io.github.resilience4j</groupId>
			<artifactId>resilience4j-retry</artifactId>
			<version>${resilience4j.version}</version>
		</dependency>

		<dependency>
			<groupId>org.projectlombok</groupId>
			<artifactId>lombok</artifactId>
			<!-- <version>${lombok.version}</version> -->
			<scope>provided</scope>
		</dependency>
		
		<!-- COMMONS-LANG -->
		<dependency>
		    <groupId>org.apache.commons</groupId>
		    <artifactId>commons-lang3</artifactId>
		    <version>${commons-lang3.version}</version><!--$NO-MVN-MAN-VER$-->
		</dependency>
		
		<!-- KUMO-CORE-COMMONS -->
		<dependency>
			<groupId>com.fsegura.kumo-core</groupId>
			<artifactId>kumo-core-commons</artifactId>
			<version>${kumo-core.version}</version>
		</dependency>

		<dependency>
			<groupId>org.springframework.boot</groupId>
			<artifactId>spring-boot-starter-test</artifactId>
			<scope>test</scope>
		</dependency>

		<dependency>
			<groupId>org.awaitility</groupId>
			<artifactId>awaitility</artifactId>
			<scope>test</scope>
		</dependency>

		<!-- TEST JUNIT 5 -->
		<dependency>
			<groupId>org.junit.platform</groupId>
			<artifactId>junit-platform-engine</artifactId>
			<scope>test</scope>
		</dependency>
		<dependency>
			<groupId>org.junit.platform</groupId>
			<artifactId>junit-platform-runner</artifactId>
			<scope>test</scope>
			<exclusions>
				<!-- exclude junit 4 -->
				<exclusion>
					<groupId>junit</groupId>
					<artifactId>junit</artifactId>
				</exclusion>
			</exclusions>
		</dependency>
		<dependency>
			<groupId>org.junit.jupiter</groupId>
			<artifactId>junit-jupiter-engine</artifactId>
			<scope>test</scope>
		</dependency>
		<dependency>
			<groupId>org.junit.jupiter</groupId>
			<artifactId>junit-jupiter-api</artifactId>
			<scope>test</scope>
		</dependency>
		<dependency>
			<groupId>org.junit.jupiter</groupId>
			<artifactId>junit-jupiter-params</artifactId>
			<scope>test</scope>
		</dependency>

	</dependencies>

	<dependencyManagement>
		<dependencies>
			<!-- SPRING BOOT DEPENDENCIES -->
			<dependency>
				<groupId>org.springframework.boot</groupId>
				<artifactId>spring-boot-dependencies</artifactId>
				<version>${spring-boot.version}</version>
				<type>pom</type>
				<scope>import</scope>
			</dependency>

			<!-- SPRING FRAMEWORK -->
			<!-- <dependency>
				<groupId>org.springframework</groupId>
				<artifactId>spring-framework-bom</artifactId>
				<version>${spring-framework.version}</version>
				<scope>import</scope>
				<type>pom</type>
			</dependency> -->

			<!-- SPRING CLOUD -->
			<dependency>
				<groupId>org.springframework.cloud</groupId>
				<artifactId>spring-cloud-dependencies</artifactId>
				<version>${spring-cloud.version}</version>
				<type>pom</type>
				<scope>import</scope>
			</dependency>

			<!-- AMAZON AWS SDK -->
			<dependency>
				<groupId>com.amazonaws</groupId>
				<artifactId>aws-java-sdk-bom</artifactId>
				<version>${aws-java-sdk.version}</version>
				<scope>import</scope>
				<type>pom</type>
			</dependency>
			
			<!-- MICROMETER -->
			<dependency>
				<groupId>io.micrometer</groupId>
				<artifactId>micrometer-bom</artifactId>
				<version>${micrometer.version}</version>
				<scope>import</scope>
				<type>pom</type>
			</dependency>

			<!-- JUNIT 5 -->
			<dependency>
				<groupId>org.junit</groupId>
				<artifactId>junit-bom</artifactId>
				<version>${junit-jupiter.version}</version>
				<type>pom</type>
			</dependency>
		</dependencies>
	</dependencyManagement>

	<build>
		<extensions>
			<extension>
				<groupId>ar.com.synergian</groupId>
				<artifactId>wagon-git</artifactId>
				<version>${wagon-git.version}</version>
			</extension>
		</extensions>

		<plugins>
			<plugin>
				<groupId>org.springframework.boot</groupId>
				<artifactId>spring-boot-maven-plugin</artifactId>
				<version>${spring-boot.version}</version>
			</plugin>
			<!-- BUILD HELPER -->
			<plugin>
				<groupId>org.codehaus.mojo</groupId>
				<artifactId>build-helper-maven-plugin</artifactId>
				<version>${build-helper-plugin.version}</version>
			</plugin>

			<!-- MAVEN DEPLOY PLUGIN -->
			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-deploy-plugin</artifactId>
				<version>${maven-deploy-plugin.version}</version>
			</plugin>

			<!-- MAVEN RELEASE PLUGIN -->
			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-release-plugin</artifactId>
				<version>${maven-release-plugin.version}</version>
			</plugin>

			<!-- MAVEN SUREFIRE PLUGIN -->
			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-surefire-plugin</artifactId>
				<version>${maven-surefire-plugin.version}</version>
			</plugin>

			<!-- MAVEN FAILSAFE PLUGIN -->
			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-failsafe-plugin</artifactId>
				<version>${maven-failsafe-plugin.version}</version>
			</plugin>

			<!-- MAVEN SITE PLUGIN -->
			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-site-plugin</artifactId>
				<version>${maven-site-plugin.version}</version>
			</plugin>
		</plugins>
	</build>

	<pluginRepositories>
		<pluginRepository>
			<id>spring-releases</id>
			<name>Spring GA Repository</name>
			<url>https://repo.spring.io/plugins-release/</url>
		</pluginRepository>
		<pluginRepository>
			<id>jr-ce</id>
			<name>JasperReports CE</name>
			<url>http://jaspersoft.artifactoryonline.com/jaspersoft/jr-ce-releases</url>
		</pluginRepository>
		<pluginRepository>
			<id>synergian-repo</id>
			<url>https://raw.github.com/synergian/wagon-git/releases</url>
		</pluginRepository>
	</pluginRepositories>

	<distributionManagement>
		<repository>
			<id>kumo-releases</id>
			<name>kumo releases repo</name>
			<url>git:releases://git@bitbucket.org:seguradev/kumo-maven-repository.git</url>
		</repository>
		<snapshotRepository>
			<id>kumo-snapshots</id>
			<name>kumo snapshots repo</name>
			<url>git:snapshots://git@bitbucket.org:seguradev/kumo-maven-repository.git</url>
		</snapshotRepository>
		<site>
			<id>${project.artifactId}-site</id>
			<url>${project.baseUri}</url>
		</site>
	</distributionManagement>

	<organization>
		<name>Grupo Segura</name>
		<url>https://www.fsegura.com/</url>
	</organization>

	<scm>
		<connection>scm:git:git://git@bitbucket.org:seguradev/kumo-core.git</connection>
		<developerConnection>scm:git:git://git@bitbucket.org:seguradev/kumo-core.git</developerConnection>
		<url>https://bitbucket.org/seguradev/kumo-core</url>
	</scm>

	<ciManagement>
		<system>jenkins</system>
		<url>https://jenkins.fsegura.com/</url>
	</ciManagement>

	<developers>
		<developer>
			<id>rialonso</id>
			<name>Ricardo Alonso</name>
			<email>rialonso@fsegura.com</email>
			<organization>Grupo Segura</organization>
		</developer>
	</developers>
	
	<repositories>
		<repository>
			<id>central</id>
			<name>Maven Central Repository</name>
			<url>https://repo.maven.apache.org/maven2</url>
		</repository>
		<repository>
			<id>activeeon</id>
			<name>Activeeon</name>
			<url>http://repository.activeeon.com/content/repositories/releases/</url>
		</repository>
		<repository>
	        <id>sonatype-nexus</id>
	        <name>Sonatype Nexus</name>
	        <url>https://oss.sonatype.org/content/repositories/</url>
	    </repository>
		<repository>
			<id>beLike</id>
			<name>beLike repo</name>
			<url>https://api.bitbucket.org/2.0/repositories/belike/belike-maven-repository.git/src/releases</url>
		</repository>
		<repository>
			<id>kumo-releases</id>
			<name>kumo releases repo</name>
			<url>https://api.bitbucket.org/2.0/repositories/seguradev/kumo-maven-repository.git/src/releases</url>
			<releases>
	            <enabled>true</enabled>
	        </releases>
	        <snapshots>
	            <enabled>false</enabled>
	        </snapshots>
		</repository>
	    
	    <!-- SNAPSHOTS -->
		<repository>
			<id>kumo-snapshots</id>
			<name>kumo snapshots repo</name>
			<url>https://api.bitbucket.org/2.0/repositories/seguradev/kumo-maven-repository.git/src/snapshots</url>
			<releases>
				<enabled>false</enabled>
			</releases>
			<snapshots>
				<enabled>true</enabled>
			</snapshots>
		</repository>
	</repositories>

</project>