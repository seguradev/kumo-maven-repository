<project xmlns="http://maven.apache.org/POM/4.0.0"
	xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
	xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 https://maven.apache.org/xsd/maven-4.0.0.xsd">
	<modelVersion>4.0.0</modelVersion>

	<parent>
		<groupId>com.fsegura.kumo-core</groupId>
		<artifactId>kumo-core</artifactId>
		<version>0.0.42-SNAPSHOT</version>
	</parent>

	<artifactId>kumo-core-vo-sap</artifactId>
	<name>kumo-core-vo-sap</name>
	<packaging>jar</packaging>

	<properties>
		<jaxb.base-path.generated>${project.build.directory}/generated-sources/jaxb</jaxb.base-path.generated>
	</properties>
	
	<dependencies>
		<!-- JAXB LocalDateTime Adapter -->
		<!-- https://github.com/threeten-jaxb/threeten-jaxb -->
		<dependency>
			<groupId>io.github.threeten-jaxb</groupId>
			<artifactId>threeten-jaxb-core</artifactId>
			<version>${threeten-jaxb.version}</version>
		</dependency>
		<!-- Only version 2.0.0 -->
		<!-- <dependency>
			<groupId>io.github.threeten-jaxb</groupId>
			<artifactId>threeten-jaxb-extra</artifactId>
			<version>${threeten-jaxb.version}</version>
		</dependency> -->
		
		<!-- APACHE CXF RUNTIME -->
		<dependency>
		   <groupId>org.apache.cxf.xjc-utils</groupId>
		   <artifactId>cxf-xjc-runtime</artifactId>
		   <version>${apache.cxf-xjc.version}</version>
		</dependency>
		
		<dependency>
			<groupId>org.slf4j</groupId>
			<artifactId>slf4j-api</artifactId>
		</dependency>
		
		<!-- For JAX-WS Maven Plugin -->
		<dependency>
			<groupId>com.sun.xml.ws</groupId>
			<artifactId>jaxws-rt</artifactId>
			<!-- <scope>runtime</scope> -->
		</dependency>
		
		<dependency>
			<groupId>org.glassfish.jaxb</groupId>
			<artifactId>jaxb-xjc</artifactId>
		</dependency>
		<dependency>
			<groupId>org.glassfish.jaxb</groupId>
			<artifactId>jaxb-runtime</artifactId>
			<!-- <scope>runtime</scope> -->
		</dependency>
		
		<!-- to compile xjc-generated sources -->
	    <dependency>
	    	<groupId>org.jvnet.jaxb2_commons</groupId>
	    	<artifactId>jaxb2-basics-runtime</artifactId>
	    	<version>${jaxb2-basics.version}</version>
	    </dependency>
	</dependencies>
	
	<build>
		<resources>
			<resource>
				<directory>src/main/resources</directory>
				<filtering>true</filtering>
				<includes>
					<include>**/*.xsd</include>
					<include>**/*.wsdl</include>
					<include>**/*.xjb</include>
				</includes>
				<excludes>
					<exclude>**/schemas/bindingschema_2_0.xsd</exclude>
					<exclude>**/schemas/bindingschema_3_0.xsd</exclude>
					<exclude>**/schemas/wsdl_customizationschema_2_0.xsd</exclude>
				</excludes>
			</resource>
		</resources>

		<plugins>
			<!-- APACHE CXF CODEGEN PLUGIN-->
			<!-- https://cxf.apache.org/docs/maven-cxf-codegen-plugin-wsdl-to-java.html -->
			<!-- <plugin>
				<groupId>org.apache.cxf</groupId>
				<artifactId>cxf-codegen-plugin</artifactId>
				<version>${apache.cxf.version}</version>
				<executions>
					<execution>
						<id>generate-sources</id>
						<phase>generate-sources</phase>
						<configuration>
							<sourceRoot>${jaxb.base-path.generated}/cxf</sourceRoot>
							<defaultOptions>
								<markGenerated>true</markGenerated>
							 	<suppressGeneratedDate>true</suppressGeneratedDate>
							 	<extendedSoapHeaders>true</extendedSoapHeaders>
							 	<allowElementRefs>true</allowElementRefs>
							 	<autoNameResolution>true</autoNameResolution>
							 	<noAddressBinding>true</noAddressBinding>
							 	
							 	<bindingFiles>
							 		<bindingFile>${project.basedir}/src/main/resources/bindings/global.xjb</bindingFile>
							 	</bindingFiles>
							 	
							 	<extraargs>
							 		<extraarg>-xjc-XautoNameResolution</extraarg>
							 	</extraargs>
							</defaultOptions>
							
							<wsdlOptions>
								<wsdlOption>
									<outputDir>${jaxb.base-path.generated}/jaxws/iface19</outputDir>
									<bindingFiles>
										<bindingFile>${project.basedir}/src/main/resources/bindings/jaxb.bindings.iface_13_SchedulingAgreementDelivery.xjb</bindingFile>
                						<bindingFile>${project.basedir}/src/main/resources/bindings/jaxws.bindings.iface_13_SchedulingAgreementDelivery.xjb</bindingFile>
									</bindingFiles>
									<wsdl>${basedir}/src/main/resources/wsdl/IFACE_13_SCHEDGAGRMTDLIVSCHEDULE_IN.wsdl</wsdl>
									<wsdlLocation>classpath:wsdl/IFACE_13_SCHEDGAGRMTDLIVSCHEDULE_IN.wsdl</wsdlLocation>
								</wsdlOption>
								
								<wsdlOption>
									<outputDir>${jaxb.base-path.generated}/jaxws/iface13</outputDir>
									<bindingFiles>
										<bindingFile>${project.basedir}/src/main/resources/bindings/jaxws.bindings.iface_19_BillingDocumentRequest.xjb</bindingFile>
                						<bindingFile>${project.basedir}/src/main/resources/bindings/jaxb.bindings.iface_19_BillingDocumentRequest.xjb</bindingFile>
									</bindingFiles>
									<wsdl>${basedir}/src/main/resources/wsdl/IFACE_19_BDR_REQUEST_IN.wsdl</wsdl>
									<wsdlLocation>classpath:wsdl/IFACE_19_BDR_REQUEST_IN.wsdl</wsdlLocation>
								</wsdlOption>
								
								<wsdlOption>
									<outputDir>${jaxb.base-path.generated}/jaxws/iface40</outputDir>
									<bindingFiles>
										<bindingFile>${project.basedir}/src/main/resources/bindings/jaxws.bindings.iface_40_Autofactura.xjb</bindingFile>
                						<bindingFile>${project.basedir}/src/main/resources/bindings/jaxb.bindings.iface_40_Autofactura.xjb</bindingFile>
									</bindingFiles>
									<wsdl>${basedir}/src/main/resources/wsdl/IFACE_40_AUTOFACTURA.wsdl</wsdl>
									<wsdlLocation>classpath:wsdl/IFACE_40_AUTOFACTURA.wsdl</wsdlLocation>
								</wsdlOption>
							</wsdlOptions>
						</configuration>
						<goals>
							<goal>wsdl2java</goal>
						</goals>
					</execution>
				</executions>
			</plugin> -->
			
			<!-- Jakarta XML Web Services -->
			<!-- JAX-WS Maven Plugin -->
			<!-- https://github.com/eclipse-ee4j/metro-jax-ws -->
			<plugin>
				<groupId>com.sun.xml.ws</groupId>
				<artifactId>jaxws-maven-plugin</artifactId>
				<configuration>
					<bindingDirectory>${project.basedir}/src/main/resources/bindings/</bindingDirectory>
                	<bindingFiles>
						<bindingFile>${project.basedir}/src/main/resources/bindings/global.xjb</bindingFile>
					</bindingFiles>
					
					<wsdlDirectory>${project.basedir}/src/main/resources/wsdl/</wsdlDirectory>
					
					<extension>true</extension>
					
					<xjcArgs>
						<xjcArg>-mark-generated</xjcArg>
		             	<xjcArg>-nv</xjcArg>
		             	<xjcArg>-npa</xjcArg>
						<xjcArg>-no-header</xjcArg>
		                <!-- <xjcArg>-XautoNameResolution</xjcArg> -->
						<!-- <xjcArg>-Xnamespace-prefix</xjcArg> -->
		            	<xjcArg>-Xannotate</xjcArg>
		            	<xjcArg>-XremoveAnnotation</xjcArg>
		            </xjcArgs>
                	
                	<vmArgs>
                		<vmArg>-Djavax.xml.accessExternalSchema=all</vmArg>
                		<vmArg>-Dcom.sun.tools.xjc.XJCFacade.nohack=true</vmArg>
                	</vmArgs>
					
					<xnocompile>true</xnocompile>
                	<keep>false</keep>
                	
					<genJWS>false</genJWS>
                	<xdisableAuthenticator>true</xdisableAuthenticator>
                	<xuseBaseResourceAndURLToLoadWSDL>false</xuseBaseResourceAndURLToLoadWSDL>
                	
                	<verbose>true</verbose>
                	<xdebug>false</xdebug>
                	
                	<detail>true</detail>
					
				</configuration>
				<executions>
					<execution>
                		<id>import-iface-13</id>
                		<goals>
                			<goal>wsimport</goal>
                		</goals>
                		<configuration>
                			<sourceDestDir>${jaxb.base-path.generated}/jaxws/iface13</sourceDestDir>
                			<bindingFiles>
                				<bindingFile>jaxws.bindings.iface_13_SchedulingAgreementDelivery.xjb</bindingFile>
                				<bindingFile>jaxb.bindings.iface_13_SchedulingAgreementDelivery.xjb</bindingFile>
                			</bindingFiles>
                			<wsdlFiles>
                				<wsdlFile>IFACE_13_SCHEDGAGRMTDLIVSCHEDULE_IN.wsdl</wsdlFile>
                			</wsdlFiles>
                			<wsdlLocation>classpath:wsdl/IFACE_13_SCHEDGAGRMTDLIVSCHEDULE_IN.wsdl</wsdlLocation>
                		</configuration>
                	</execution>
                	<execution>
                		<id>import-iface-19</id>
                		<goals>
                			<goal>wsimport</goal>
                		</goals>
                		<configuration>
                			<sourceDestDir>${jaxb.base-path.generated}/jaxws/iface19</sourceDestDir>
                			<bindingFiles>
                				<bindingFile>jaxws.bindings.iface_19_BillingDocumentRequest.xjb</bindingFile>
                				<bindingFile>jaxb.bindings.iface_19_BillingDocumentRequest.xjb</bindingFile>
                			</bindingFiles>
                			<wsdlFiles>
                				<wsdlFile>IFACE_19_BDR_REQUEST_IN.wsdl</wsdlFile>
                			</wsdlFiles>
                			<wsdlLocation>classpath:wsdl/IFACE_19_BDR_REQUEST_IN.wsdl</wsdlLocation>
                		</configuration>
                	</execution>
                	<execution>
                		<id>import-iface-40</id>
                		<goals>
                			<goal>wsimport</goal>
                		</goals>
                		<configuration>
                			<sourceDestDir>${jaxb.base-path.generated}/jaxws/iface40</sourceDestDir>
                			<bindingFiles>
                				<bindingFile>jaxws.bindings.iface_40_Autofactura.xjb</bindingFile>
                				<bindingFile>jaxb.bindings.iface_40_Autofactura.xjb</bindingFile>
                			</bindingFiles>
                			<wsdlFiles>
                				<wsdlFile>IFACE_40_AUTOFACTURA.wsdl</wsdlFile>
                			</wsdlFiles>
                			<wsdlLocation>classpath:wsdl/IFACE_40_AUTOFACTURA.wsdl</wsdlLocation>
                		</configuration>
                	</execution>
				</executions>
			</plugin>
			
			<!-- JVNET JAXB2 -->
           	<plugin>
                <groupId>org.jvnet.jaxb2.maven2</groupId>
                <artifactId>maven-jaxb2-plugin</artifactId>
                <configuration>
                    <schemaDirectory>${project.basedir}/src/main/resources/</schemaDirectory>
                    <schemaExcludes>
                        <schemaExclude>**/bindingschema_2_0.xsd</schemaExclude>
                        <schemaExclude>**/bindingschema_3_0.xsd</schemaExclude>
                        <schemaExclude>**/wsdl_customizationschema_2_0.xsd</schemaExclude>
                    </schemaExcludes>
                    
                    <bindingDirectory>${project.basedir}/src/main/resources/bindings/</bindingDirectory>
                    
                    <args>
                        <arg>-nv</arg>
                        <arg>-npa</arg>
                        <arg>-no-header</arg>
                        <!-- <arg>-Xnamespace-prefix</arg> -->
                        <arg>-Xannotate</arg>
                        <arg>-XremoveAnnotation</arg>
                        <arg>-Xfluent-api</arg>
                    </args>
                    
                    <removeOldOutput>true</removeOldOutput>
                    
                    <scanDependenciesForBindings>true</scanDependenciesForBindings>
                    <useDependenciesAsEpisodes>true</useDependenciesAsEpisodes>
                    <extension>true</extension>
                    <noFileHeader>true</noFileHeader>
                    <enableIntrospection>true</enableIntrospection>
                    <disableXmlSecurity>true</disableXmlSecurity>
                    <accessExternalSchema>all</accessExternalSchema>
                    <accessExternalDTD>all</accessExternalDTD>
                    <verbose>true</verbose>
                </configuration>
                <executions>
                    <execution>
                        <id>generate-from-xsd</id>
                        <goals>
                            <goal>generate</goal>
                        </goals>
                        <configuration>
                            <generateDirectory>${jaxb.base-path.generated}/jaxb</generateDirectory>
                            <schemaIncludes>
                                <schemaInclude>**/*.xsd</schemaInclude>
                            </schemaIncludes>
                            <bindingIncludes>
                                <bindingInclude>jaxb.bindings.iface_33_productionOrderConf.xjb</bindingInclude>
                                <bindingInclude>jaxb.bindings.iface_33b_cancelationConfirmation.xjb</bindingInclude>
                                <bindingInclude>jaxb.bindings.iface_50_accountingEntryMessage.xjb</bindingInclude>
                            </bindingIncludes>
                        </configuration>
                    </execution>
                </executions>
            </plugin>
			
			<!-- MAVEN BUILD HELPER -->
			<plugin>
	        	<groupId>org.codehaus.mojo</groupId>
	        	<artifactId>build-helper-maven-plugin</artifactId>
	        	<executions>
	        		<execution>
	        			<id>add-source</id>
            			<phase>generate-sources</phase>
            			<goals>
			                <goal>add-source</goal>
			            </goals>
	        		</execution>
	        	</executions>
	        	<configuration>
	        		<sources>
	        			<source>${jaxb.base-path.generated}/jaxws/iface13</source>
	        			<source>${jaxb.base-path.generated}/jaxws/iface19</source>
	        			<source>${jaxb.base-path.generated}/jaxws/iface40</source>
	        			<source>${jaxb.base-path.generated}/jaxb</source>
	        		</sources>
	        	</configuration>
			</plugin>
		</plugins>
		
		<pluginManagement>
			<plugins>
				<!-- Jakarta XML Web Services -->
				<!-- JAX-WS Maven Plugin -->
				<plugin>
					<groupId>com.sun.xml.ws</groupId>
					<artifactId>jaxws-maven-plugin</artifactId>
					<version>${com.sun.xml.ws.version}</version>
					<dependencies>
						<dependency>
							<groupId>com.sun.xml.ws</groupId>
							<artifactId>jaxws-tools</artifactId>
							<version>${com.sun.xml.ws.version}</version>
						</dependency>
						<dependency>
							<groupId>org.slf4j</groupId>
							<artifactId>slf4j-simple</artifactId>
							<version>${slf4j.version}</version>
						</dependency>
						<!-- put xjc-plugins on the jaxws-maven-plugin's classpath -->
						<!-- https://github.com/highsource/jaxb2-basics -->
						<dependency>
							<groupId>org.jvnet.jaxb2_commons</groupId>
							<artifactId>jaxb2-basics</artifactId>
							<version>${jaxb2-basics.version}</version>
						</dependency>
						<!-- <dependency>
							<groupId>org.jvnet.jaxb2_commons</groupId>
							<artifactId>jaxb2-namespace-prefix</artifactId>
							<version>${jaxb2-namespace-prefix.version}</version>
						</dependency> -->
						<dependency>
							<groupId>org.jvnet.jaxb2_commons</groupId>
							<artifactId>jaxb2-basics-annotate</artifactId>
							<version>${jaxb2-basics-annotate.version}</version>
						</dependency>
					</dependencies>
				</plugin>
				
				<!-- JVNET JAXB2 -->
				<plugin>
					<groupId>org.jvnet.jaxb2.maven2</groupId>
					<artifactId>maven-jaxb2-plugin</artifactId>
					<version>${maven-jaxb2-plugin-jvnet.version}</version>
					<dependencies>
						<dependency>
							<groupId>com.sun.xml.bind</groupId>
							<artifactId>jaxb-impl</artifactId>
							<version>${com.sun.xml.ws.version}</version>
						</dependency>
						<dependency>
							<groupId>com.sun.xml.bind</groupId>
							<artifactId>jaxb-xjc</artifactId>
							<version>${com.sun.xml.ws.version}</version>
						</dependency>						
						<dependency>
							<groupId>org.slf4j</groupId>
							<artifactId>slf4j-simple</artifactId>
							<version>${slf4j.version}</version>
						</dependency>
						<dependency>
							<groupId>org.jvnet.jaxb2_commons</groupId>
							<artifactId>jaxb2-basics</artifactId>
							<version>${jaxb2-basics.version}</version>
						</dependency>
						<!-- <dependency>
							<groupId>org.jvnet.jaxb2_commons</groupId>
							<artifactId>jaxb2-namespace-prefix</artifactId>
							<version>${jaxb2-namespace-prefix.version}</version>
						</dependency> -->
						<dependency>
							<groupId>org.jvnet.jaxb2_commons</groupId>
							<artifactId>jaxb2-basics-annotate</artifactId>
							<version>${jaxb2-basics-annotate.version}</version>
						</dependency>
						<!-- https://github.com/dmak/jaxb-xew-plugin -->
						<!-- <dependency>
							<groupId>com.github.jaxb-xew-plugin</groupId>
						    <artifactId>jaxb-xew-plugin</artifactId>
						    <version>${jaxb-xew.version}</version>
						</dependency> -->
					</dependencies>
					<configuration>
	                	<plugins>
							<plugin>
					            <groupId>net.java.dev.jaxb2-commons</groupId>
					            <artifactId>jaxb-fluent-api</artifactId>
					            <version>${jaxb-fluent-api.version}</version>
					        </plugin>
						</plugins>
	                </configuration>
				</plugin>
			</plugins>
		</pluginManagement>
	</build>
	
	<!-- <profiles>
		<profile>
			<id>jdk11</id>
	        <activation>
	            <jdk>[11,)</jdk>
	        </activation>
	        <dependencies>
	        	<dependency>
	        		<groupId>com.sun.xml.ws</groupId>
	        		<artifactId>jaxws-ri</artifactId>
	        		<version>${com.sun.xml.version}</version>
	        		<type>pom</type>
	        	</dependency>
	        	<dependency>
	        		<groupId>com.sun.xml.ws</groupId>
	        		<artifactId>rt</artifactId>
	        		<version>${com.sun.xml.ws.version}</version>
	        	</dependency>
	        </dependencies>
		</profile>
	</profiles> -->
</project>