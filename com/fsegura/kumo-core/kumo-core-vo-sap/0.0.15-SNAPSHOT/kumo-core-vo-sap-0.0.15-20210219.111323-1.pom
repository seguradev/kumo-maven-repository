<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 https://maven.apache.org/xsd/maven-4.0.0.xsd">
	<modelVersion>4.0.0</modelVersion>
	
	<parent>
		<groupId>com.fsegura.kumo-core</groupId>
		<artifactId>kumo-core</artifactId>
		<version>0.0.15-SNAPSHOT</version>
	</parent>
	
	<artifactId>kumo-core-vo-sap</artifactId>
	<name>kumo-core-vo-sap</name>
	<packaging>jar</packaging>
	
	<properties>
		<jaxb.base-path.generated>${project.build.directory}/generated-sources/jaxb</jaxb.base-path.generated>
	</properties>
	
	<dependencies>
		<!-- JAXB LocalDateTime Adapter -->
		<dependency>
		    <groupId>com.migesok</groupId>
		    <artifactId>jaxb-java-time-adapters</artifactId>
		    <version>${jaxb-java-time-adapters.version}</version>
		</dependency>
		
		<!-- CXF XJC RUNTIME -->
		<!-- <dependency>
		   <groupId>org.apache.cxf.xjc-utils</groupId>
		   <artifactId>cxf-xjc-runtime</artifactId>
		   <version>${cxf-xjc.version}</version>
		</dependency> -->
		
		<!-- to compile xjc-generated sources -->
	    <dependency>
	    	<groupId>org.jvnet.jaxb2_commons</groupId>
	    	<artifactId>jaxb2-basics-runtime</artifactId>
	    	<version>${jaxb2-basics.version}</version>
	    </dependency>
	    
	</dependencies>
	
	<build>
		<resources>
			<resource>
				<directory>src/main/resources</directory>
				<filtering>true</filtering>
				<includes>
					<include>**/*.xsd</include>
					<include>**/*.wsdl</include>
					<include>**/*.xjb</include>
				</includes>
				<excludes>
					<exclude>**/bindingschema_2_0.xsd</exclude>
				</excludes>
			</resource>
		</resources>
		
		<plugins>
			<!-- JVNET JAX-WS -->
			<plugin>
				<groupId>org.jvnet.jax-ws-commons</groupId>
				<artifactId>jaxws-maven-plugin</artifactId>
				<configuration>
					<bindingDirectory>${project.basedir}/src/main/resources/bindings/</bindingDirectory>
                	<bindingFiles>
						<bindingFile>global.xjb</bindingFile>
					</bindingFiles>
					
					<wsdlDirectory>${project.basedir}/src/main/resources/wsdl/</wsdlDirectory>
                	
                	<extension>true</extension>
		            
		             <xjcArgs>
		             	<!-- <xjcArg>-mark-generated</xjcArg> -->
		             	<xjcArg>-nv</xjcArg> <!-- do not perform strict validation of the input schema(s) -->
		             	<xjcArg>-npa</xjcArg> <!-- suppress generation of package level annotations (**/package-info.java) -->
		            	<!-- <xjcArg>-debug</xjcArg> -->
						<xjcArg>-no-header</xjcArg>
		            	<!-- <xjcArg>-Xxew</xjcArg>
						<xjcArg>-Xxew:instantiate lazy</xjcArg>
						<xjcArg>-Xxew:control src/main/resources/wsdl/classes-control.txt</xjcArg> -->
                        <!-- <xjcArg>-Xfluent-api</xjcArg> -->
		                <xjcArg>-XautoNameResolution</xjcArg>
						<xjcArg>-Xnamespace-prefix</xjcArg>
		            	<xjcArg>-Xannotate</xjcArg>
		            	<xjcArg>-XremoveAnnotation</xjcArg>
		            </xjcArgs>
                	
                	<vmArgs>
                		<vmArg>-Djavax.xml.accessExternalSchema=all</vmArg>
                		<vmArg>-Dcom.sun.tools.xjc.XJCFacade.nohack=true</vmArg>
                	</vmArgs>
                	
                	<xnocompile>true</xnocompile>
                	<keep>true</keep>
                	
					<genJWS>false</genJWS>
                	<xdisableAuthenticator>true</xdisableAuthenticator>
                	<xuseBaseResourceAndURLToLoadWSDL>false</xuseBaseResourceAndURLToLoadWSDL>
                	
                	<verbose>true</verbose>
                	<xdebug>false</xdebug>
		            
				</configuration>
				<executions>
					<execution>
                		<id>import-iface-13</id>
                		<goals>
                			<goal>wsimport</goal>
                		</goals>
                		<configuration>
                			<sourceDestDir>${jaxb.base-path.generated}/jaxws/iface13</sourceDestDir>
                			<bindingFiles>
                				<bindingFile>jaxws.bindings.iface_13_SchedulingAgreementDelivery.xjb</bindingFile>
                				<bindingFile>jaxb.bindings.iface_13_SchedulingAgreementDelivery.xjb</bindingFile>
                			</bindingFiles>
                			<wsdlFiles>
                				<wsdlFile>IFACE_13_SCHEDGAGRMTDLIVSCHEDULE_IN.wsdl</wsdlFile>
                			</wsdlFiles>
                		</configuration>
                	</execution>
					<execution>
                		<id>import-iface-19</id>
                		<goals>
                			<goal>wsimport</goal>
                		</goals>
                		<configuration>
                			<sourceDestDir>${jaxb.base-path.generated}/jaxws/iface19</sourceDestDir>
                			<bindingFiles>
                				<bindingFile>jaxws.bindings.iface_19_BillingDocumentRequest.xjb</bindingFile>
                				<bindingFile>jaxb.bindings.iface_19_BillingDocumentRequest.xjb</bindingFile>
                			</bindingFiles>
                			<wsdlFiles>
                				<wsdlFile>IFACE_19_BDR_REQUEST_IN.wsdl</wsdlFile>
                			</wsdlFiles>
                		</configuration>
                	</execution>
                	<execution>
                		<id>import-iface-40</id>
                		<goals>
                			<goal>wsimport</goal>
                		</goals>
                		<configuration>
                			<sourceDestDir>${jaxb.base-path.generated}/jaxws/iface40</sourceDestDir>
                			<bindingFiles>
                				<bindingFile>jaxws.bindings.iface_40_Autofactura.xjb</bindingFile>
                				<bindingFile>jaxb.bindings.iface_40_Autofactura.xjb</bindingFile>
                			</bindingFiles>
                			<wsdlFiles>
                				<wsdlFile>IFACE_40_AUTOFACTURA.wsdl</wsdlFile>
                			</wsdlFiles>
                		</configuration>
                	</execution>
                </executions>
			</plugin>
            
            <!-- JVNET JAXB2 -->
           	<plugin>
                <groupId>org.jvnet.jaxb2.maven2</groupId>
                <artifactId>maven-jaxb2-plugin</artifactId>
                <configuration>
                    <schemaDirectory>${project.basedir}/src/main/resources/</schemaDirectory>
                    <schemaExcludes>
                        <schemaExclude>**/bindingschema_2_0.xsd</schemaExclude>
                    </schemaExcludes>
                    
                    <bindingDirectory>${project.basedir}/src/main/resources/bindings/</bindingDirectory>
                    
                    <args>
                        <arg>-nv</arg>
                        <arg>-npa</arg>
                        <arg>-no-header</arg>
                        <arg>-Xnamespace-prefix</arg>
                        <arg>-Xannotate</arg>
                        <arg>-XremoveAnnotation</arg>
                        <arg>-Xfluent-api</arg>
                    </args>
                    
                    <removeOldOutput>true</removeOldOutput>
                    <!-- <readOnly>true</readOnly> -->
                    
                    <scanDependenciesForBindings>true</scanDependenciesForBindings>
                    <useDependenciesAsEpisodes>true</useDependenciesAsEpisodes>
                    <extension>true</extension>
                    <noFileHeader>true</noFileHeader>
                    <enableIntrospection>true</enableIntrospection>
                    <disableXmlSecurity>true</disableXmlSecurity>
                    <accessExternalSchema>all</accessExternalSchema>
                    <accessExternalDTD>all</accessExternalDTD>
                    <verbose>true</verbose>
                </configuration>
                <executions>
                    <execution>
                        <id>generate-from-xsd</id>
                        <goals>
                            <goal>generate</goal>
                        </goals>
                        <configuration>
                            <generateDirectory>${jaxb.base-path.generated}/jaxb</generateDirectory>
                            <schemaIncludes>
                                <schemaInclude>**/*.xsd</schemaInclude>
                            </schemaIncludes>
                            <bindingIncludes>
                                <bindingInclude>jaxb.bindings.iface_33_productionOrderConf.xjb</bindingInclude>
                                <bindingInclude>jaxb.bindings.iface_33b_cancelationConfirmation.xjb</bindingInclude>
                                <bindingInclude>jaxb.bindings.iface_50_accountingEntryMessage.xjb</bindingInclude>
                            </bindingIncludes>
                        </configuration>
                    </execution>
                </executions>
            </plugin>
            
            <!-- APACHE CXF CODEGEN -->
			<!-- https://github.com/apache/cxf/blob/master/maven-plugins/codegen-plugin/src/main/java/org/apache/cxf/maven_plugin/wsdl2java/Option.java -->
			<!-- <plugin>
				<groupId>org.apache.cxf</groupId>
				<artifactId>cxf-codegen-plugin</artifactId>
				<executions>
					<execution>
						<id>generate-sources</id>
						<phase>generate-sources</phase>
						<configuration>
							<sourceRoot>${cfx.generated}</sourceRoot>
							<defaultOptions>
								<markGenerated>false</markGenerated>
								<suppressGeneratedDate>true</suppressGeneratedDate>
								<bindingFiles>
									<bindingFile>\${project.basedir}/src/main/resources/wsdl/bindings/global.xjb</bindingFile>
								</bindingFiles>
								<extraargs>
								    <extraarg>-xjc-Xannotate</extraarg>
								    <extraarg>-xjc-XremoveAnnotation</extraarg>
								    <extraarg>-xjc-XautoNameResolution</extraarg>
								    <extraarg>-xjc-Xnamespace-prefix</extraarg>
								    <extraarg>-mark-generated</extraarg>
								</extraargs>
							</defaultOptions>
							<wsdlOptions>
								<wsdlOption>
									<wsdl>${project.basedir}/src/main/resources/wsdl/sap/IFACE_13_SCHEDGAGRMTDLIVSCHEDULE_IN.wsdl</wsdl>
									<wsdlLocation>classpath:wsdl/sap/IFACE_13_SCHEDGAGRMTDLIVSCHEDULE_IN.wsdl</wsdlLocation>
									<bindingFiles>
										<bindingFile>${project.basedir}/src/main/resources/wsdl/bindings/jaxws.bindings.iface_13_SchedulingAgreementDelivery.xjb</bindingFile>
										<bindingFile>${project.basedir}/src/main/resources/wsdl/bindings/jaxb.bindings.iface_13_SchedulingAgreementDelivery.xjb</bindingFile>
									</bindingFiles>
								</wsdlOption>
								<wsdlOption>
									<wsdl>${project.basedir}/src/main/resources/wsdl/sap/IFACE_19_BDR_REQUEST_IN.wsdl</wsdl>
									<wsdlLocation>classpath:wsdl/sap/IFACE_19_BDR_REQUEST_IN.wsdl</wsdlLocation>
									<bindingFiles>
										<bindingFile>${project.basedir}/src/main/resources/wsdl/bindings/jaxws.bindings.iface_19_BillingDocumentRequest.xjb</bindingFile>
										<bindingFile>${project.basedir}/src/main/resources/wsdl/bindings/jaxb.bindings.iface_19_BillingDocumentRequest.xjb</bindingFile>
									</bindingFiles>
								</wsdlOption>
								<wsdlOption>
									<wsdl>${project.basedir}/src/main/resources/wsdl/sap/IFACE_40_AUTOFACTURA.wsdl</wsdl>
									<wsdlLocation>classpath:wsdl/sap/IFACE_40_AUTOFACTURA.wsdl</wsdlLocation>
									<bindingFiles>
										<bindingFile>${project.basedir}/src/main/resources/wsdl/bindings/jaxws.bindings.iface_40_Autofactura.xjb</bindingFile>
										<bindingFile>${project.basedir}/src/main/resources/wsdl/bindings/jaxb.bindings.iface_40_Autofactura.xjb</bindingFile>
									</bindingFiles>
								</wsdlOption>
							</wsdlOptions>
						</configuration>
						<goals>
							<goal>wsdl2java</goal>
						</goals>
					</execution>
				</executions>
			</plugin> -->
			
			<!-- MAVEN BUILD HELPER -->
			<plugin>
	        	<groupId>org.codehaus.mojo</groupId>
	        	<artifactId>build-helper-maven-plugin</artifactId>
	        	<executions>
	        		<execution>
	        			<id>add-source</id>
            			<phase>generate-sources</phase>
            			<goals>
			                <goal>add-source</goal>
			            </goals>
	        		</execution>
	        	</executions>
	        	<configuration>
	        		<sources>
	        			<source>${jaxb.base-path.generated}/jaxws/iface13</source>
	        			<source>${jaxb.base-path.generated}/jaxws/iface19</source>
	        			<source>${jaxb.base-path.generated}/jaxws/iface40</source>
	        			<source>${jaxb.base-path.generated}/jaxb</source>
	        		</sources>
	        	</configuration>
			</plugin>
		</plugins>
		
		<pluginManagement>
			<plugins>
				<!-- JAXB2 -->
				<plugin>
					<groupId>org.jvnet.jaxb2.maven2</groupId>
					<artifactId>maven-jaxb2-plugin</artifactId>
					<version>${maven-jaxb2-plugin.version}</version>
	                <inherited>true</inherited>
					<dependencies>
						<dependency>
							<groupId>org.slf4j</groupId>
							<artifactId>slf4j-simple</artifactId>
							<version>${slf4j.version}</version>
						</dependency>
						<dependency>
							<groupId>org.jvnet.jaxb2_commons</groupId>
							<artifactId>jaxb2-namespace-prefix</artifactId>
							<version>${jaxb2-namespace-prefix.version}</version>
						</dependency>
	                </dependencies>
	                <configuration>
	                	<plugins>
							<plugin>
								<groupId>org.jvnet.jaxb2_commons</groupId>
								<artifactId>jaxb2-basics</artifactId>
								<version>${jaxb2-basics.version}</version>
							</plugin>
							<plugin>
								<groupId>org.jvnet.jaxb2_commons</groupId>
			                	<artifactId>jaxb2-basics-annotate</artifactId>
			                	<version>${jaxb2-basics-annotate.version}</version>
							</plugin>
							<plugin>
								<groupId>org.jvnet.jaxb2_commons</groupId>
								<artifactId>jaxb2-basics-tools</artifactId>
								<version>${jaxb2-basics.version}</version>
							</plugin>
							<plugin>
					            <groupId>net.java.dev.jaxb2-commons</groupId>
					            <artifactId>jaxb-fluent-api</artifactId>
					            <version>${jaxb-fluent-api.version}</version>
					        </plugin>
					        <!-- https://github.com/dmak/jaxb-xew-plugin -->
							<plugin>
								<groupId>com.github.jaxb-xew-plugin</groupId>
							    <artifactId>jaxb-xew-plugin</artifactId>
							    <version>${jaxb-xew.version}</version>
							</plugin>
						</plugins>
	                </configuration>
				</plugin>
				
				<!-- JVNET JAX-WS -->
				<plugin>
					<groupId>org.jvnet.jax-ws-commons</groupId>
					<artifactId>jaxws-maven-plugin</artifactId>
					<version>${jaxws-maven-plugin.version}</version>
					<inherited>true</inherited>
					<dependencies>
						<dependency>
						    <groupId>com.sun.xml.ws</groupId>
						    <artifactId>jaxws-tools</artifactId>
						    <version>${com.sun.xml.version}</version>
						</dependency>
	                	<dependency>
							<groupId>org.slf4j</groupId>
							<artifactId>slf4j-simple</artifactId>
							<version>${slf4j.version}</version>
						</dependency>
						<dependency>
	                      	<groupId>org.jvnet.jaxb2_commons</groupId>
	                       	<artifactId>jaxb2-basics</artifactId>
	                       	<version>${jaxb2-basics.version}</version>
						</dependency>
						<dependency>
							<groupId>org.jvnet.jaxb2_commons</groupId>
							<artifactId>jaxb2-namespace-prefix</artifactId>
							<version>${jaxb2-namespace-prefix.version}</version>
						</dependency>
						<dependency>
							<groupId>org.jvnet.jaxb2_commons</groupId>
							<artifactId>jaxb2-basics-annotate</artifactId>
							<version>${jaxb2-basics-annotate.version}</version>
						</dependency>
						<dependency>
							<groupId>org.jvnet.jaxb2_commons</groupId>
							<artifactId>jaxb2-basics-tools</artifactId>
							<version>${jaxb2-basics.version}</version>
						</dependency>
					</dependencies>
					<configuration>
	                	<plugins>
							<plugin>
								<groupId>org.jvnet.jaxb2_commons</groupId>
								<artifactId>jaxb2-basics</artifactId>
								<version>${jaxb2-basics.version}</version>
							</plugin>
							<plugin>
								<groupId>org.jvnet.jaxb2_commons</groupId>
			                	<artifactId>jaxb2-basics-annotate</artifactId>
			                	<version>${jaxb2-basics-annotate.version}</version>
							</plugin>
							<plugin>
								<groupId>org.jvnet.jaxb2_commons</groupId>
								<artifactId>jaxb2-basics-tools</artifactId>
								<version>${jaxb2-basics.version}</version>
							</plugin>
							<plugin>
					            <groupId>net.java.dev.jaxb2-commons</groupId>
					            <artifactId>jaxb-fluent-api</artifactId>
					            <version>${jaxb-fluent-api.version}</version>
					        </plugin>
							<plugin>
								<groupId>com.github.jaxb-xew-plugin</groupId>
							    <artifactId>jaxb-xew-plugin</artifactId>
							    <version>${jaxb-xew.version}</version>
							</plugin>
						</plugins>
	                </configuration>
				</plugin>
				
				<!-- JAXB2 XML/XSD GENERATOR -->
				<!-- <plugin>
		            <groupId>org.codehaus.mojo</groupId>
		            <artifactId>jaxb2-maven-plugin</artifactId>
		            <version>${jaxb2-maven-plugin.version}</version>
	            </plugin> -->
	            
				<!-- JAXWS MAVEN PLUGIN -->
				<!-- <plugin>
					<groupId>org.codehaus.mojo</groupId>
                	<artifactId>jaxws-maven-plugin</artifactId>
                	<version>${jaxws-plugin.version}</version>
                	<inherited>true</inherited>
					<dependencies>
						<dependency>
                        	<groupId>org.jvnet.jaxb2_commons</groupId>
                        	<artifactId>jaxb2-basics</artifactId>
                        	<version>${jaxb2-basics.version}</version>
						</dependency>
	                    <dependency>
	                        <groupId>org.jvnet.jaxb2_commons</groupId>
	                        <artifactId>jaxb2-namespace-prefix</artifactId>
	                        <version>${jaxb2-namespace-prefix.version}</version>
	                    </dependency>
	                    <dependency>
	                    	<groupId>org.jvnet.jaxb2_commons</groupId>
	                    	<artifactId>jaxb2-basics-annotate</artifactId>
	                    	<version>${jaxb2-basics-annotate.version}</version>
	                    </dependency>
	                    <dependency>
	                    	<groupId>org.jvnet.jaxb2_commons</groupId>
							<artifactId>jaxb2-basics-tools</artifactId>
							<version>${jaxb2-basics.version}</version>
	                    </dependency>
	                    <dependency>
						    <groupId>com.github.jaxb-xew-plugin</groupId>
						    <artifactId>jaxb-xew-plugin</artifactId>
						    <version>${jaxb-xew.version}</version>
						</dependency>
	                </dependencies>
	                <configuration>
	                	<plugins>
							<plugin>
								<groupId>org.jvnet.jaxb2_commons</groupId>
								<artifactId>jaxb2-basics</artifactId>
								<version>${jaxb2-basics.version}</version>
							</plugin>
							<plugin>
								 <groupId>org.jvnet.jaxb2_commons</groupId>
	                        	<artifactId>jaxb2-namespace-prefix</artifactId>
	                        	<version>${jaxb2-namespace-prefix.version}</version>
							</plugin>
							<plugin>
								<groupId>org.jvnet.jaxb2_commons</groupId>
			                	<artifactId>jaxb2-basics-annotate</artifactId>
			                	<version>${jaxb2-basics-annotate.version}</version>
							</plugin>
							<plugin>
								<groupId>org.jvnet.jaxb2_commons</groupId>
								<artifactId>jaxb2-basics-tools</artifactId>
								<version>${jaxb2-basics.version}</version>
							</plugin>
							<plugin>
								<groupId>com.github.jaxb-xew-plugin</groupId>
							    <artifactId>jaxb-xew-plugin</artifactId>
							    <version>${jaxb-xew.version}</version>
							</plugin>
						</plugins>
	                </configuration>
                </plugin> -->
                
				<!-- APACHE CXF -->
				<plugin>
					<groupId>org.apache.cxf</groupId>
					<artifactId>cxf-codegen-plugin</artifactId>
					<version>${apache.cxf.version}</version>
					<inherited>true</inherited>
					<dependencies>
						<dependency>
                        	<groupId>org.jvnet.jaxb2_commons</groupId>
                        	<artifactId>jaxb2-basics</artifactId>
                        	<version>${jaxb2-basics.version}</version>
						</dependency>
	                    <dependency>
	                        <groupId>org.jvnet.jaxb2_commons</groupId>
	                        <artifactId>jaxb2-namespace-prefix</artifactId>
	                        <version>${jaxb2-namespace-prefix.version}</version>
	                    </dependency>
	                    <dependency>
	                    	<groupId>org.jvnet.jaxb2_commons</groupId>
	                    	<artifactId>jaxb2-basics-annotate</artifactId>
	                    	<version>${jaxb2-basics-annotate.version}</version>
	                    </dependency>
	                    <dependency>
						    <groupId>com.github.jaxb-xew-plugin</groupId>
						    <artifactId>jaxb-xew-plugin</artifactId>
						    <version>${jaxb-xew.version}</version>
						</dependency>
						<dependency>
	                    	<groupId>net.java.dev.jaxb2-commons</groupId>
	                    	<artifactId>jaxb-fluent-api</artifactId>
	                    	<version>${jaxb-fluent-api.version}</version>
	                    </dependency>
	                </dependencies>
				</plugin>
			</plugins>
		</pluginManagement>
		
	</build>
	
</project>