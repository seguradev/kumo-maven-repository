<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
	<modelVersion>4.0.0</modelVersion>
	
	<parent>
		<groupId>com.fsegura.kumo-core</groupId>
		<artifactId>kumo-core</artifactId>
		<version>0.0.45-SNAPSHOT</version>
	</parent>
	
	<artifactId>kumo-core-dao</artifactId>
	<name>kumo-core-dao</name>
	<packaging>jar</packaging>
	
	<properties>
		<query-dsl.generated>${project.build.directory}/generated-sources/java</query-dsl.generated>
		<hibernate-ddl.generated>${project.build.directory}/generated-resources/sql/ddl</hibernate-ddl.generated>
	</properties>
	
	<dependencies>
		<!-- SPRING DATA JPA -->
		<dependency>
			<groupId>org.springframework.data</groupId>
			<artifactId>spring-data-jpa</artifactId>
		</dependency>
		<!-- SPRING SECURITY -->
		<dependency>
		    <groupId>org.springframework.security</groupId>
		    <artifactId>spring-security-core</artifactId>
		</dependency>
		
		<!-- PERSISTENCE-API -->
        <dependency>
    		<groupId>jakarta.persistence</groupId>
    		<artifactId>jakarta.persistence-api</artifactId>
    		<!-- jakarta.jakartaee-bom:8.0.0 tiene mal definida la versión de esta dependencia -->
    		<!-- <version>${jakarta.persistence-api.version}</version> -->
		</dependency>
		<!-- VALIDATION-API -->
		<dependency>
    		<groupId>jakarta.validation</groupId>
    		<artifactId>jakarta.validation-api</artifactId>
		</dependency>
		<!-- ANNOTATION-API -->
		<dependency>
			<groupId>jakarta.annotation</groupId>
			<artifactId>jakarta.annotation-api</artifactId>
		</dependency>
		
		<!-- BELIKE-CORE-DATA -->
		<dependency>
			<groupId>com.belike.belike-core</groupId>
			<artifactId>belike-core-data</artifactId>
		</dependency>
		<dependency>
			<groupId>com.belike.belike-core</groupId>
			<artifactId>belike-core-data-querydsl</artifactId>
		</dependency>
		
		<!-- KUMO-CORE-COMMONS -->
		<dependency>
			<groupId>com.fsegura.kumo-core</groupId>
			<artifactId>kumo-core-commons</artifactId>
			<version>${project.version}</version>
		</dependency>
		<dependency>
			<groupId>com.fsegura.kumo-core</groupId>
			<artifactId>kumo-core-error</artifactId>
			<version>${project.version}</version>
		</dependency>
		<dependency>
			<groupId>com.fsegura.kumo-core</groupId>
			<artifactId>kumo-core-dao-commons</artifactId>
			<version>${project.version}</version>
		</dependency>
		<dependency>
			<groupId>com.fsegura.kumo-core</groupId>
			<artifactId>kumo-core-util</artifactId>
			<version>${project.version}</version>
		</dependency>
		
		<!-- POSTGRESQL -->
		<dependency>
			<groupId>org.postgresql</groupId>
			<artifactId>postgresql</artifactId>
			<scope>runtime</scope>
		</dependency>
		
		<!-- QUERYDSL -->
		<dependency>
			<groupId>com.querydsl</groupId>
			<artifactId>querydsl-core</artifactId>
			<exclusions>
				<exclusion>
					<groupId>com.google.code.findbugs</groupId>
					<artifactId>jsr305</artifactId>
				</exclusion>
			</exclusions>
		</dependency>
		<dependency>
            <groupId>com.querydsl</groupId>
            <artifactId>querydsl-codegen</artifactId>
        </dependency>
		<dependency>
			<groupId>com.querydsl</groupId>
			<artifactId>querydsl-jpa</artifactId>
		</dependency>
		<dependency>
			<groupId>com.querydsl</groupId>
			<artifactId>querydsl-collections</artifactId>
		</dependency>
		
		<!-- HIBERNATE JCACHE -->
		<dependency>
			<groupId>org.hibernate</groupId>
			<artifactId>hibernate-jcache</artifactId>
		</dependency>
		<dependency>
			<groupId>javax.cache</groupId>
			<artifactId>cache-api</artifactId>
		</dependency>
		
		<!-- HIBERNATE ENVERS -->
        <dependency>
			<groupId>org.hibernate</groupId>
			<artifactId>hibernate-envers</artifactId>
		</dependency>
		
		<!-- HIBERNATE SEARCH -->
        <dependency>
			<groupId>org.hibernate.search</groupId>
			<artifactId>hibernate-search-mapper-orm</artifactId>
			<version>${hibernate-search.version}</version>
		</dependency>
		<!-- HIBERNATE SEARCH ELASTICSEARCH -->
		<!-- <dependency>
			<groupId>org.hibernate.search</groupId>
			<artifactId>hibernate-search-backend-elasticsearch</artifactId>
			<version>${hibernate-search.version}</version>
		</dependency> -->
		<!-- HIBERNATE SEARCH ELASTICSEARCH AWS -->
		<!-- <dependency>
			<groupId>org.hibernate.search</groupId>
			<artifactId>hibernate-search-backend-elasticsearch-aws</artifactId>
			<version>${hibernate-search.version}</version>
		</dependency> -->
		
		<!-- ELASTICSEARCH -->
		<!-- <dependency>
			<groupId>org.elasticsearch.client</groupId>
			<artifactId>elasticsearch-rest-high-level-client</artifactId>
		</dependency> -->
		
		<!-- <dependency>
			<groupId>com.belike.belike-core</groupId>
			<artifactId>belike-core-elasticsearch</artifactId>
			<version>${belike-core.version}</version>
		</dependency> -->
		
		<!-- HIBERNATE TYPES -->
		<!-- https://github.com/vladmihalcea/hypersistence-utils -->
		<!-- https://vladmihalcea.com/multidimensional-array-jpa-hibernate/ -->		
		<dependency>
			<groupId>io.hypersistence</groupId>
			<artifactId>hypersistence-utils-hibernate-55</artifactId>
			<version>${hypersistence-utils.version}</version>
		</dependency>
		
		<dependency>
			<groupId>com.fasterxml.jackson.core</groupId>
			<artifactId>jackson-databind</artifactId>
		</dependency>
		
		<dependency>
			<groupId>com.fasterxml.jackson.module</groupId>
			<artifactId>jackson-module-jaxb-annotations</artifactId>
		</dependency>
		
		<!-- APACHE COMMONS -->
		<dependency>
			<groupId>org.apache.commons</groupId>
			<artifactId>commons-lang3</artifactId>
		</dependency>
		
	</dependencies>
	
	<profiles>
		<profile>
			<id>local</id>
			<properties>
				<name>env</name>
				<value>local</value>
				<spring.profiles.active>local</spring.profiles.active>
				<!-- <app.filter>src/main/resources/application-local-${user.name}.properties</app.filter> -->
				<!-- <spring.profiles.active>local-${user.name}</spring.profiles.active> -->
			</properties>
			<activation>
				<activeByDefault>true</activeByDefault>
			</activation>
		</profile>
		<profile>
			<id>test</id>
			<properties>
				<name>env</name>
				<value>test</value>
				<spring.profiles.active>test</spring.profiles.active>
			</properties>
		</profile>
		<profile>
			<id>pro</id>
			<properties>
				<name>env</name>
				<value>pro</value>
				<spring.profiles.active>pro</spring.profiles.active>
			</properties>
		</profile>
	</profiles>
	
	<build>
		<resources>
			<resource>
				<directory>src/main/resources</directory>
				<filtering>true</filtering>
				<includes>
					<include>**/*.sql</include>
					<include>**/*.xml</include>
				</includes>
				<excludes>
					<exclude>**/*.properties</exclude>
				</excludes>
			</resource>
			<!-- <resource>
				<directory>src/main/resources</directory>
				<filtering>false</filtering>
				<includes>
					<include>**/*.xml</include>
				</includes>
			</resource> -->
		</resources>
		
		<plugins>
			<!-- HIBERNATE 5.X DDL GENERATOR -->
		   	<plugin>
			   	<groupId>de.jpdigital</groupId>
		   		<artifactId>hibernate56-ddl-maven-plugin</artifactId>
			   	<configuration>
	               	<customDialects>
                    	<param>org.hibernate.dialect.PostgreSQL10Dialect</param>
                	</customDialects>
	               	<packages>
	                   	<param>com.fsegura.kumo.core.dao.entities</param>
	               	</packages>
	               	<outputDirectory>${hibernate-ddl.generated}</outputDirectory>
			   	</configuration>
			</plugin>
			
			<!-- QUERY DSL GENERATOR APT -->
			<plugin>
				<groupId>com.mysema.maven</groupId>
				<artifactId>apt-maven-plugin</artifactId>
				<executions>
					<execution>
						<phase>generate-sources</phase>
						<goals>
							<goal>process</goal>
						</goals>
						<configuration>
							<outputDirectory>${query-dsl.generated}</outputDirectory>
							<processors>
								<processor>com.querydsl.apt.jpa.JPAAnnotationProcessor</processor>
								<processor>lombok.launch.AnnotationProcessorHider$AnnotationProcessor</processor>
							</processors>
							<options>
								<querydsl.entityAccessors>true</querydsl.entityAccessors>
								<querydsl.generatedAnnotationClass>javax.annotation.Generated</querydsl.generatedAnnotationClass>
							</options>
						</configuration>
					</execution>
				</executions>
			</plugin>
			
			<!-- Hibernate Enhance Maven Plugin -->
			<!-- https://vladmihalcea.com/maven-gradle-hibernate-enhance-plugin/ -->
			<plugin>
				 <groupId>org.hibernate.orm.tooling</groupId>
				 <artifactId>hibernate-enhance-maven-plugin</artifactId>
				 <version>${hibernate.version}</version>
				 <executions>
				 	<execution>
				 		<configuration>
				 			<enableLazyInitialization>true</enableLazyInitialization>
				 			<enableDirtyTracking>true</enableDirtyTracking>
				 			<enableAssociationManagement>true</enableAssociationManagement>
				 			<enableExtendedEnhancement>false</enableExtendedEnhancement>
				 		</configuration>
				 		<goals>
				 			<goal>enhance</goal>
				 		</goals>
				 	</execution>
				 </executions>
			</plugin>
			
			
			<!-- QUERY DSL GENERATOR -->
			<!-- <plugin>
				<groupId>com.querydsl</groupId>
				<artifactId>querydsl-maven-plugin</artifactId>
				<executions>
					<execution>
						<goals>
							<goal>jpa-export</goal>
						</goals>
					</execution>
				</executions>
				<configuration>
					<packages>
						<package>com.fsegura.kumo.core.dao.entities</package>
					</packages>
          			<targetFolder>${query-dsl.generated}</targetFolder>
				</configuration>
			</plugin> -->
			
			<!-- FLYWAY -->
			<plugin>
				<groupId>org.flywaydb</groupId>
				<artifactId>flyway-maven-plugin</artifactId>
				<dependencies>
					<dependency>
						<groupId>org.postgresql</groupId>
						<artifactId>postgresql</artifactId>
						<version>${postgresql.version}</version>
					</dependency>
				</dependencies>
				<configuration>
					<configFiles>						
						<configFile>src/main/resources/flyway-${spring.profiles.active}.properties</configFile>
					</configFiles>
				</configuration>
			</plugin>
			
			<!-- MAVEN BUILD HELPER -->
			<plugin>
				<groupId>org.codehaus.mojo</groupId>
	        	<artifactId>build-helper-maven-plugin</artifactId>
	        	<executions>
	        		<execution>
	        			<id>add-source</id>
            			<phase>generate-sources</phase>
            			<goals>
			                <goal>add-source</goal>
			            </goals>
	        		</execution>
	        	</executions>
	        	<configuration>
	        		<sources>
	        			<source>${query-dsl.generated}</source>
	        		</sources>
	        	</configuration>
	        </plugin>
	        
	        <!-- SPOTBUGS MAVEN PLUGIN -->
			<plugin>
				<groupId>com.github.spotbugs</groupId>
				<artifactId>spotbugs-maven-plugin</artifactId>
				<configuration>
					<excludeFilterFile>${basedir}/src/main/resources/spotbugs/spotbugs-exclude.xml</excludeFilterFile>
				</configuration>
			</plugin>
			
			<!-- MAVEN REMOTE RESOURCES PLUGIN -->
			<!-- <plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-remote-resources-plugin</artifactId>
				<executions>
					<execution>
						<goals>
							<goal>bundle</goal>
						</goals>
					</execution>
				</executions>
				<configuration>
					<resourcesDirectory>${basedir}/src/main/resources/spotbugs</resourcesDirectory>
					<includes>
						<include>**/spotbugs-exclude-dao.xml</include>
					</includes>
				</configuration>
			</plugin> -->
			
		</plugins>
	</build>
</project>