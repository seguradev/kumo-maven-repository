<?xml version="1.0" encoding="UTF-8"?>
<project
	xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd"
	xmlns="http://maven.apache.org/POM/4.0.0"
	xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
	<modelVersion>4.0.0</modelVersion>
	
	<parent>
		<groupId>com.fsegura.kumo-core</groupId>
		<artifactId>kumo-core</artifactId>
		<version>0.0.1-SNAPSHOT</version>
	</parent>
	
	<artifactId>kumo-core-dao</artifactId>
	<name>kumo-core-dao</name>
	<packaging>jar</packaging>
	
	<properties>
		<query-dsl.generated>${project.build.directory}/generated-sources/java</query-dsl.generated>
		<hibernate-ddl.generated>${project.build.directory}/generated-resources/sql/ddl</hibernate-ddl.generated>
	</properties>
	
	<dependencies>
		<!-- SPRING CONFIG PROCESSOR -->
		<dependency>
			<groupId>org.springframework.boot</groupId>
			<artifactId>spring-boot-configuration-processor</artifactId>
			<optional>true</optional>
		</dependency>
		<!-- SPRING DATA JPA -->
		<dependency>
			<groupId>org.springframework.boot</groupId>
			<artifactId>spring-boot-starter-data-jpa</artifactId>
		</dependency>
		<!-- SPRING SECURITY -->
		<dependency>
			<groupId>org.springframework.boot</groupId>
			<artifactId>spring-boot-starter-security</artifactId>
		</dependency>
		
		<!-- PERSISTENCE -->
        <dependency>
    		<groupId>jakarta.persistence</groupId>
    		<artifactId>jakarta.persistence-api</artifactId>
    		<scope>provided</scope>
		</dependency>
		<!-- VALIDATION -->
		<dependency>
    		<groupId>jakarta.validation</groupId>
    		<artifactId>jakarta.validation-api</artifactId>
		</dependency>
		
		<!-- BELIKE-CORE-DATA -->
		<dependency>
			<groupId>com.belike.belike-core</groupId>
			<artifactId>belike-core-data</artifactId>
		</dependency>
		<dependency>
			<groupId>com.belike.belike-core</groupId>
			<artifactId>belike-core-data-querydsl</artifactId>
		</dependency>
		
		<!-- KUMO-CORE-COMMONS -->
		<dependency>
			<groupId>com.fsegura.kumo-core</groupId>
			<artifactId>kumo-core-commons</artifactId>
			<version>${project.version}</version>
		</dependency>
		
		<!-- POSTGRESQL -->
		<dependency>
			<groupId>org.postgresql</groupId>
			<artifactId>postgresql</artifactId>
		</dependency>
		
		<!-- QUERYDSL -->
		<dependency>
			<groupId>com.querydsl</groupId>
			<artifactId>querydsl-jpa</artifactId>
		</dependency>
		<dependency>
			<groupId>com.querydsl</groupId>
			<artifactId>querydsl-collections</artifactId>
		</dependency>
		
		<!-- HIBERNATE EHCACHE 3 -->
		<dependency>
		     <groupId>org.hibernate</groupId>
		     <artifactId>hibernate-jcache</artifactId>
		</dependency>
		<dependency>
		    <groupId>org.hibernate</groupId>
		    <artifactId>hibernate-ehcache</artifactId>
		</dependency>
		<dependency>
		    <groupId>org.ehcache</groupId>
		    <artifactId>ehcache</artifactId>
		    <scope>runtime</scope>
		</dependency>
		
		<!-- HIBERNATE ENVERS -->
        <dependency>
			<groupId>org.hibernate</groupId>
			<artifactId>hibernate-envers</artifactId>
		</dependency>
		
		<!-- HIBERNATE SEARCH -->
        <dependency>
			<groupId>org.hibernate</groupId>
			<artifactId>hibernate-search-orm</artifactId>
			<version>${hibernate-search.version}</version>
		</dependency>
		<!-- HIBERNATE SEARCH ELASTICSEARCH -->
		<dependency>
			<groupId>org.hibernate</groupId>
			<artifactId>hibernate-search-elasticsearch</artifactId>
			<version>${hibernate-search.version}</version>
		</dependency>
		<!-- HIBERNATE SEARCH ELASTICSEARCH AWS -->
		<!-- <dependency>
			<groupId>org.hibernate</groupId>
			<artifactId>hibernate-search-elasticsearch-aws</artifactId>
			<version>${hibernate-search.version}</version>
		</dependency> -->
		
		<!-- ELASTICSEARCH -->
		<!-- <dependency>
			<groupId>org.elasticsearch.client</groupId>
			<artifactId>elasticsearch-rest-high-level-client</artifactId>
		</dependency> -->
		
		<dependency>
			<groupId>com.belike.belike-core</groupId>
			<artifactId>belike-core-elasticsearch</artifactId>
			<version>${belike-core.version}</version>
		</dependency>
		
		<dependency>
			<groupId>com.fasterxml.jackson.core</groupId>
			<artifactId>jackson-databind</artifactId>
			<!-- <SCOPE>TEST</SCOPE> -->
		</dependency>
		
		<!-- COMMONS LANG -->
		<dependency>
			<groupId>org.apache.commons</groupId>
			<artifactId>commons-lang3</artifactId>
		</dependency>
		
	</dependencies>
	
	<profiles>
		<profile>
			<id>local</id>
			<properties>
				<name>env</name>
				<value>local</value>
				<spring.profiles.active>local</spring.profiles.active>
				<!-- <app.filter>src/main/resources/application-local-${user.name}.properties</app.filter> -->
				<!-- <spring.profiles.active>local-${user.name}</spring.profiles.active> -->
			</properties>
			<activation>
				<activeByDefault>true</activeByDefault>
			</activation>
		</profile>
		<profile>
			<id>test</id>
			<properties>
				<name>env</name>
				<value>test</value>
				<spring.profiles.active>test</spring.profiles.active>
			</properties>
		</profile>
		<profile>
			<id>pro</id>
			<properties>
				<name>env</name>
				<value>pro</value>
				<spring.profiles.active>pro</spring.profiles.active>
			</properties>
		</profile>
	</profiles>
	
	<build>
		<resources>
			<resource>
				<directory>src/main/resources</directory>
				<filtering>true</filtering>
				<includes>
					<include>**/*.properties</include>
					<include>**/*.sql</include>
				</includes>
			</resource>
		</resources>
		
		<plugins>
			<!-- HIBERNATE 5.4.x DDL GENERATOR -->
		   	<plugin>
			   	<groupId>de.jpdigital</groupId>
		   		<artifactId>hibernate54-ddl-maven-plugin</artifactId>
			   	<configuration>
	               	<customDialects>
                    	<param>org.hibernate.dialect.PostgreSQL10Dialect</param>
                	</customDialects>
	               	<packages>
	                   	<param>com.fsegura.kumo.core.dao.entities</param>
	               	</packages>
	               	<outputDirectory>${hibernate-ddl.generated}</outputDirectory>
			   	</configuration>
			</plugin>
			
			<!-- QUERY DSL GENERATOR -->
			<plugin>
				<groupId>com.mysema.maven</groupId>
				<artifactId>apt-maven-plugin</artifactId>
				<executions>
					<execution>
						<goals>
							<goal>process</goal>
						</goals>
						<configuration>
							<outputDirectory>${query-dsl.generated}</outputDirectory>
							<processor>com.querydsl.apt.jpa.JPAAnnotationProcessor</processor>
							<options>
								<querydsl.entityAccessors>true</querydsl.entityAccessors>
							</options>
						</configuration>
					</execution>
				</executions>
			</plugin>
			
			<!-- FLYWAY -->
			<plugin>
				<groupId>org.flywaydb</groupId>
				<artifactId>flyway-maven-plugin</artifactId>
				<dependencies>
					<!-- JDBC -->
					<dependency>
						<groupId>org.postgresql</groupId>
						<artifactId>postgresql</artifactId>
						<version>${postgresql.version}</version>
					</dependency>
				</dependencies>
				<configuration>
					<configFiles>						
						<configFile>src/main/resources/flyway-${spring.profiles.active}.properties</configFile>
					</configFiles>
				</configuration>
			</plugin>
			
			<!-- MAVEN BUILD HELPER -->
			 <plugin>
	        	<groupId>org.codehaus.mojo</groupId>
	        	<artifactId>build-helper-maven-plugin</artifactId>
	        	<executions>
	        		<execution>
	        			<id>add-source</id>
            			<phase>generate-sources</phase>
            			<goals>
			                <goal>add-source</goal>
			            </goals>
	        		</execution>
	        	</executions>
	        	<configuration>
	        		<sources>
	        			<source>${query-dsl.generated}</source>
	        		</sources>
	        	</configuration>
			</plugin>
			
		</plugins>
	</build>
</project>