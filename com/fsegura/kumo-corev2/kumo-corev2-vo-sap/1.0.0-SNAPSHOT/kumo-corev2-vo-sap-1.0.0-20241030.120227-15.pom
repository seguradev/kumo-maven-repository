<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0"
         xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
    <modelVersion>4.0.0</modelVersion>
    <parent>
        <groupId>com.fsegura.kumo-corev2</groupId>
        <artifactId>kumo-corev2</artifactId>
        <version>1.0.0-SNAPSHOT</version>
    </parent>

    <artifactId>kumo-corev2-vo-sap</artifactId>

    <properties>
        <com.sun.xml.ws.version>4.0.2</com.sun.xml.ws.version>
        <hisrc-basicjaxb.version>2.2.1</hisrc-basicjaxb.version>
        <threeten-jaxb.version>2.2.0</threeten-jaxb.version>

        <!-- PLUGINS -->
        <jaxws-maven-plugin.version>${com.sun.xml.ws.version}</jaxws-maven-plugin.version>
        <hisrc-higherjaxb-maven-plugin.version>${hisrc-basicjaxb.version}</hisrc-higherjaxb-maven-plugin.version>

        <!-- PATHS -->
        <jaxb.base-path.generated>${project.build.directory}/generated-sources/jaxb</jaxb.base-path.generated>
    </properties>

    <dependencies>
        <dependency>
            <groupId>jakarta.xml.bind</groupId>
            <artifactId>jakarta.xml.bind-api</artifactId>
            <!-- <version>4.0.1</version> -->
        </dependency>

        <!-- <dependency>
            <groupId>org.glassfish.jaxb</groupId>
            <artifactId>jaxb-runtime</artifactId>
            <version>4.0.4</version>
        </dependency> -->

        <!-- For JAX-WS Maven Plugin -->
        <dependency>
            <groupId>com.sun.xml.ws</groupId>
            <artifactId>jaxws-rt</artifactId>
            <version>${com.sun.xml.ws.version}</version>
            <!-- <scope>runtime</scope> -->
        </dependency>

        <!-- HiSrc BasicJAXB -->
        <dependency>
            <groupId>org.patrodyne.jvnet</groupId>
            <artifactId>hisrc-basicjaxb-runtime</artifactId>
            <version>${hisrc-basicjaxb.version}</version>
        </dependency>

        <!-- JAXB LocalDateTime Adapter -->
        <!-- https://github.com/threeten-jaxb/threeten-jaxb -->
        <!-- https://mvnrepository.com/artifact/io.github.threeten-jaxb -->
        <dependency>
            <groupId>io.github.threeten-jaxb</groupId>
            <artifactId>threeten-jaxb-core</artifactId>
            <version>${threeten-jaxb.version}</version>
        </dependency>
        <dependency>
            <groupId>io.github.threeten-jaxb</groupId>
            <artifactId>threeten-jaxb-extra</artifactId>
            <version>${threeten-jaxb.version}</version>
        </dependency>
    </dependencies>

    <build>
        <resources>
            <resource>
                <directory>src/main/resources</directory>
                <filtering>false</filtering>
                <includes>
                    <include>**/*.xsd</include>
                    <include>**/*.wsdl</include>
                    <include>**/*.xjb</include>
                </includes>
                <excludes>
                    <exclude>**/schemas/bindingschema_3_0.xsd</exclude>
                    <exclude>**/schemas/wsdl_customizationschema_3_0.xsd</exclude>
                </excludes>
            </resource>
        </resources>

        <plugins>
            <!-- Jakarta XML Web Services -->
            <!-- JAX-WS Maven Plugin -->
            <!-- https://github.com/eclipse-ee4j/metro-jax-ws -->
            <!-- https://eclipse-ee4j.github.io/metro-jax-ws/jaxws-maven-plugin/ -->
            <!-- https://mvnrepository.com/artifact/com.sun.xml.ws/jaxws-maven-plugin -->
            <plugin>
                <groupId>com.sun.xml.ws</groupId>
                <artifactId>jaxws-maven-plugin</artifactId>
                <configuration>
                    <bindingDirectory>${project.basedir}/src/main/resources/bindings/</bindingDirectory>
                    <bindingFiles>
                        <bindingFile>${project.basedir}/src/main/resources/bindings/global.xjb</bindingFile>
                    </bindingFiles>

                    <wsdlDirectory>${project.basedir}/src/main/resources/wsdl/</wsdlDirectory>

                    <extension>true</extension>

                    <xjcArgs>
                        <xjcArg>-mark-generated</xjcArg>
                        <xjcArg>-nv</xjcArg>
                        <xjcArg>-npa</xjcArg>
                        <xjcArg>-no-header</xjcArg>
                        <!-- <xjcArg>-XautoNameResolution</xjcArg> -->
                        <!-- <xjcArg>-Xnamespace-prefix</xjcArg> -->
                        <xjcArg>-Xannotate</xjcArg>
                        <xjcArg>-XremoveAnnotation</xjcArg>
                    </xjcArgs>

                    <vmArgs>
                        <vmArg>-Djavax.xml.accessExternalSchema=all</vmArg>
                        <vmArg>-Dcom.sun.tools.xjc.XJCFacade.nohack=true</vmArg>
                    </vmArgs>

                    <xnocompile>true</xnocompile>
                    <keep>false</keep>

                    <genJWS>false</genJWS>
                    <xdisableAuthenticator>true</xdisableAuthenticator>
                    <xuseBaseResourceAndURLToLoadWSDL>false</xuseBaseResourceAndURLToLoadWSDL>

                    <verbose>true</verbose>
                    <xdebug>false</xdebug>

                    <detail>true</detail>
                </configuration>
                <executions>
                    <execution>
                        <id>import-iface-13</id>
                        <goals>
                            <goal>wsimport</goal>
                        </goals>
                        <configuration>
                            <sourceDestDir>${jaxb.base-path.generated}/jaxws/iface13</sourceDestDir>
                            <bindingFiles>
                                <bindingFile>jaxws.bindings.iface_13_SchedulingAgreementDelivery.xjb</bindingFile>
                                <bindingFile>jaxb.bindings.iface_13_SchedulingAgreementDelivery.xjb</bindingFile>
                            </bindingFiles>
                            <wsdlFiles>
                                <wsdlFile>IFACE_13_SCHEDGAGRMTDLIVSCHEDULE_IN.wsdl</wsdlFile>
                            </wsdlFiles>
                            <wsdlLocation>classpath:wsdl/IFACE_13_SCHEDGAGRMTDLIVSCHEDULE_IN.wsdl</wsdlLocation>
                        </configuration>
                    </execution>
                    <execution>
                        <id>import-iface-19</id>
                        <goals>
                            <goal>wsimport</goal>
                        </goals>
                        <configuration>
                            <sourceDestDir>${jaxb.base-path.generated}/jaxws/iface19</sourceDestDir>
                            <bindingFiles>
                                <bindingFile>jaxws.bindings.iface_19_BillingDocumentRequest.xjb</bindingFile>
                                <bindingFile>jaxb.bindings.iface_19_BillingDocumentRequest.xjb</bindingFile>
                            </bindingFiles>
                            <wsdlFiles>
                                <wsdlFile>IFACE_19_BDR_REQUEST_IN.wsdl</wsdlFile>
                            </wsdlFiles>
                            <wsdlLocation>classpath:wsdl/IFACE_19_BDR_REQUEST_IN.wsdl</wsdlLocation>
                        </configuration>
                    </execution>
                    <execution>
                        <id>import-iface-40</id>
                        <goals>
                            <goal>wsimport</goal>
                        </goals>
                        <configuration>
                            <sourceDestDir>${jaxb.base-path.generated}/jaxws/iface40</sourceDestDir>
                            <bindingFiles>
                                <bindingFile>jaxws.bindings.iface_40_Autofactura.xjb</bindingFile>
                                <bindingFile>jaxb.bindings.iface_40_Autofactura.xjb</bindingFile>
                            </bindingFiles>
                            <wsdlFiles>
                                <wsdlFile>IFACE_40_AUTOFACTURA.wsdl</wsdlFile>
                            </wsdlFiles>
                            <wsdlLocation>classpath:wsdl/IFACE_40_AUTOFACTURA.wsdl</wsdlLocation>
                        </configuration>
                    </execution>
                    <execution>
                        <id>import-iface-y</id>
                        <goals>
                            <goal>wsimport</goal>
                        </goals>
                        <configuration>
                            <sourceDestDir>${jaxb.base-path.generated}/jaxws/ifaceY</sourceDestDir>
                            <bindingFiles>
                                <bindingFile>jaxws.bindings.iface_y_SalesOrderBulkRequest.xjb</bindingFile>
                                <bindingFile>jaxb.bindings.iface_y_SalesOrderBulkRequest.xjb</bindingFile>
                            </bindingFiles>
                            <wsdlFiles>
                                <wsdlFile>IFACE_Y_SALESORDERBULKREQUEST_IN.wsdl</wsdlFile>
                            </wsdlFiles>
                            <wsdlLocation>classpath:wsdl/IFACE_Y_SALESORDERBULKREQUEST_IN.wsdl</wsdlLocation>
                        </configuration>
                    </execution>
                </executions>
            </plugin>

            <!-- HiSrc HigherJAXB Maven Plugin -->
            <!-- https://github.com/patrodyne/hisrc-hyperjaxb -->
            <plugin>
                <groupId>org.patrodyne.jvnet</groupId>
                <artifactId>hisrc-higherjaxb-maven-plugin</artifactId>
                <configuration>
                    <schemaDirectory>${project.basedir}/src/main/resources/</schemaDirectory>
                    <schemaExcludes>
                        <schemaExclude>**/bindingschema_3_0.xsd</schemaExclude>
                        <schemaExclude>**/wsdl_customizationschema_3_0.xsd</schemaExclude>
                    </schemaExcludes>

                    <bindingDirectory>${project.basedir}/src/main/resources/bindings/</bindingDirectory>

                    <args>
                        <arg>-nv</arg>
                        <arg>-npa</arg>
                        <arg>-no-header</arg>
                        <arg>-Xannotate</arg>
                        <arg>-XremoveAnnotation</arg>
                    </args>

                    <removeOldOutput>true</removeOldOutput>
                    <extension>true</extension>
                    <debug>true</debug>
                    <noFileHeader>true</noFileHeader>
                    <enableIntrospection>true</enableIntrospection>
                    <disableXmlSecurity>true</disableXmlSecurity>
                    <accessExternalSchema>all</accessExternalSchema>
                    <accessExternalDTD>all</accessExternalDTD>
                    <verbose>true</verbose>
                </configuration>
                <executions>
                    <execution>
                        <id>generate</id>
                        <goals>
                            <goal>generate</goal>
                        </goals>
                        <configuration>
                            <generateDirectory>${jaxb.base-path.generated}/jaxb</generateDirectory>
                            <schemaIncludes>
                                <schemaInclude>**/*.xsd</schemaInclude>
                            </schemaIncludes>
                            <bindingIncludes>
                                <bindingInclude>jaxb.bindings.iface_33_productionOrderConf.xjb</bindingInclude>
                                <bindingInclude>jaxb.bindings.iface_33b_cancelationConfirmation.xjb</bindingInclude>
                                <bindingInclude>jaxb.bindings.iface_50_accountingEntryMessage.xjb</bindingInclude>
                            </bindingIncludes>
                        </configuration>
                    </execution>
                </executions>
            </plugin>

            <!-- MAVEN BUILD HELPER -->
            <plugin>
                <groupId>org.codehaus.mojo</groupId>
                <artifactId>build-helper-maven-plugin</artifactId>
                <executions>
                    <execution>
                        <id>add-source</id>
                        <phase>generate-sources</phase>
                        <goals>
                            <goal>add-source</goal>
                        </goals>
                    </execution>
                </executions>
                <configuration>
                    <sources>
                        <source>${jaxb.base-path.generated}/jaxws/iface13</source>
                        <source>${jaxb.base-path.generated}/jaxws/iface19</source>
                        <source>${jaxb.base-path.generated}/jaxws/iface40</source>
                        <source>${jaxb.base-path.generated}/jaxws/ifaceY</source>
                        <source>${jaxb.base-path.generated}/jaxb</source>
                    </sources>
                </configuration>
            </plugin>
        </plugins>

        <pluginManagement>
            <plugins>
                <!-- Jakarta XML Web Services -->
                <!-- JAX-WS Maven Plugin -->
                <!-- https://mvnrepository.com/artifact/com.sun.xml.ws/jaxws-maven-plugin -->
                <plugin>
                    <groupId>com.sun.xml.ws</groupId>
                    <artifactId>jaxws-maven-plugin</artifactId>
                    <version>${com.sun.xml.ws.version}</version>
                    <dependencies>
                        <dependency>
                            <groupId>com.sun.xml.ws</groupId>
                            <artifactId>jaxws-tools</artifactId>
                            <version>${com.sun.xml.ws.version}</version>
                        </dependency>
                        <dependency>
                            <groupId>org.slf4j</groupId>
                            <artifactId>slf4j-simple</artifactId>
                            <version>${slf4j.version}</version>
                        </dependency>
                        <!-- put xjc-plugins on the jaxws-maven-plugin's classpath -->
                        <!-- https://github.com/patrodyne/hisrc-basicjaxb#readme -->
                        <!--  -->
                        <dependency>
                            <groupId>org.patrodyne.jvnet</groupId>
                            <artifactId>hisrc-basicjaxb-plugins</artifactId>
                            <version>${hisrc-basicjaxb.version}</version>
                        </dependency>
                        <!-- https://github.com/patrodyne/hisrc-hyperjaxb-annox#readme -->
                        <!-- https://mvnrepository.com/artifact/org.patrodyne.jvnet/hisrc-hyperjaxb-annox-plugin/ -->
                        <dependency>
                            <groupId>org.patrodyne.jvnet</groupId>
                            <artifactId>hisrc-hyperjaxb-annox-plugin</artifactId>
                            <version>${hisrc-basicjaxb.version}</version>
                        </dependency>
                    </dependencies>
                </plugin>

                <plugin>
                    <groupId>org.patrodyne.jvnet</groupId>
                    <artifactId>hisrc-higherjaxb-maven-plugin</artifactId>
                    <version>${hisrc-higherjaxb-maven-plugin.version}</version>
                    <configuration>
                        <plugins>
                            <plugin>
                                <groupId>org.patrodyne.jvnet</groupId>
                                <artifactId>hisrc-basicjaxb-plugins</artifactId>
                                <version>${hisrc-basicjaxb.version}</version>
                            </plugin>
                            <plugin>
                                <groupId>org.patrodyne.jvnet</groupId>
                                <artifactId>hisrc-hyperjaxb-annox-plugin</artifactId>
                                <version>${hisrc-basicjaxb.version}</version>
                            </plugin>
                        </plugins>
                    </configuration>
                </plugin>
            </plugins>
        </pluginManagement>

    </build>

</project>