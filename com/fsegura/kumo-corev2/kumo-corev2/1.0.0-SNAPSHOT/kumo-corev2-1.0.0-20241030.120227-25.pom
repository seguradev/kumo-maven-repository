<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0"
         xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
    <modelVersion>4.0.0</modelVersion>

    <groupId>com.fsegura.kumo-corev2</groupId>
    <artifactId>kumo-corev2</artifactId>
    <version>1.0.0-SNAPSHOT</version>
    <packaging>pom</packaging>

    <properties>
        <!-- https://maven.apache.org/maven-ci-friendly.html -->
        <!-- https://github.com/outbrain/ci-friendly-flatten-maven-plugin -->
        <revision>1.0.0-SNAPSHOT</revision>

        <skipTests>true</skipTests>

        <java.version>21</java.version>
        <maven.compiler.source>${java.version}</maven.compiler.source>
        <maven.compiler.target>${java.version}</maven.compiler.target>
        <project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
        <project.reporting.outputEncoding>UTF-8</project.reporting.outputEncoding>

        <amazon-sns-java-extended-client-lib.version>2.1.0</amazon-sns-java-extended-client-lib.version>
        <amazon-sqs-java-extended-client-lib.version>2.1.1</amazon-sqs-java-extended-client-lib.version>
        <awssdk.version>2.26.30</awssdk.version>
        <commons.collections4.version>4.4</commons.collections4.version>
        <commons-lang3.version>3.14.0</commons-lang3.version>
        <guava.version>33.3.1-jre</guava.version>
        <hibernate.version>6.5.2.Final</hibernate.version>
        <hibernate-reactive.version>2.3.1.Final</hibernate-reactive.version>
        <hibernate-validator.version>8.0.1.Final</hibernate-validator.version>
        <hypersistence-utils.version>3.8.2</hypersistence-utils.version>
        <jackson.version>2.17.2</jackson.version>
        <jakarta.jakartaee.version>10.0.0</jakarta.jakartaee.version>
        <jakarta.validation-api.version>3.1.0</jakarta.validation-api.version>
        <jasperreports.version>6.21.3</jasperreports.version>
        <json.version>20240303</json.version>
        <lombok.version>1.18.34</lombok.version>
        <mapstruct.version>1.6.2</mapstruct.version>
        <opencsv.version>5.9</opencsv.version>
        <reflections.version>0.10.2</reflections.version>
        <slf4j.version>2.0.13</slf4j.version>
        <!--<spotbugs-annotations.version>4.8.6</spotbugs-annotations.version>-->
        <spring.version>6.1.13</spring.version>
        <spring-boot.version>3.3.5</spring-boot.version>
        <spring-data.version>2024.0.5</spring-data.version>
        <spring-security.version>6.3.4</spring-security.version>
        <querydsl.version>5.1.0</querydsl.version>

        <!-- PLUGINS -->
        <apt-maven-plugin.version>1.1.3</apt-maven-plugin.version>
        <build-helper-plugin.version>3.6.0</build-helper-plugin.version>
        <ci-friendly-flatten-maven-plugin.version>1.0.27</ci-friendly-flatten-maven-plugin.version>
        <maven-compiler-plugin.version>3.13.0</maven-compiler-plugin.version>
        <maven-source-plugin.version>3.3.1</maven-source-plugin.version>
        <maven-toolchains-plugin.version>3.2.0</maven-toolchains-plugin.version>
        <maven-wrapper-plugin.version>3.3.2</maven-wrapper-plugin.version>

        <!-- EXTENSIONS -->
        <wagon-git.version>0.3.0</wagon-git.version>
    </properties>

    <modules>
        <module>kumo-corev2-audit</module>
        <module>kumo-corev2-common</module>
        <module>kumo-corev2-data</module>
        <module>kumo-corev2-data-querydsl</module>
        <module>kumo-corev2-data-reactive</module>
        <module>kumo-corev2-email</module>
        <module>kumo-corev2-email-ses</module>
        <module>kumo-corev2-error</module>
        <module>kumo-corev2-logging</module>
        <module>kumo-corev2-mapper</module>
        <module>kumo-corev2-message</module>
        <module>kumo-corev2-model</module>
        <module>kumo-corev2-model-common</module>
        <module>kumo-corev2-report</module>
        <module>kumo-corev2-rest-client</module>
        <module>kumo-corev2-sync-mapper</module>
        <module>kumo-corev2-sync-util</module>
        <module>kumo-corev2-sync-vo</module>
        <module>kumo-corev2-util</module>
        <module>kumo-corev2-vo</module>
        <module>kumo-corev2-vo-mapper</module>
        <module>kumo-corev2-vo-sap</module>
        <module>kumo-corev2-vo-sync</module>
    </modules>

    <dependencies>
        <!-- LOMBOK -->
        <dependency>
            <groupId>org.projectlombok</groupId>
            <artifactId>lombok</artifactId>
            <version>${lombok.version}</version>
            <scope>provided</scope>
        </dependency>

        <dependency>
            <groupId>jakarta.validation</groupId>
            <artifactId>jakarta.validation-api</artifactId>
        </dependency>


    </dependencies>

    <dependencyManagement>
        <dependencies>
            <!-- SPRING BOOT DEPENDENCIES -->
            <dependency>
                <groupId>org.springframework.boot</groupId>
                <artifactId>spring-boot-dependencies</artifactId>
                <version>${spring-boot.version}</version>
                <scope>import</scope>
                <type>pom</type>
            </dependency>

            <!-- SPRING FRAMEWORK -->
            <dependency>
                <groupId>org.springframework</groupId>
                <artifactId>spring-framework-bom</artifactId>
                <version>${spring.version}</version>
                <scope>import</scope>
                <type>pom</type>
            </dependency>

            <!-- SPRING SECURITY -->
            <dependency>
                <groupId>org.springframework.security</groupId>
                <artifactId>spring-security-bom</artifactId>
                <version>${spring-security.version}</version>
                <scope>import</scope>
                <type>pom</type>
            </dependency>

            <!-- SPRING DATA -->
            <dependency>
                <groupId>org.springframework.data</groupId>
                <artifactId>spring-data-bom</artifactId>
                <version>${spring-data.version}</version>
                <scope>import</scope>
                <type>pom</type>
            </dependency>

            <!-- JAKARTA EE API -->
            <dependency>
                <groupId>jakarta.platform</groupId>
                <artifactId>jakarta.jakartaee-bom</artifactId>
                <version>${jakarta.jakartaee.version}</version>
                <scope>import</scope>
                <type>pom</type>
            </dependency>

            <!-- AMAZON AWS SDK 2.X -->
            <!-- https://mvnrepository.com/artifact/software.amazon.awssdk/bom -->
            <dependency>
                <groupId>software.amazon.awssdk</groupId>
                <artifactId>bom</artifactId>
                <version>${awssdk.version}</version>
                <scope>import</scope>
                <type>pom</type>
            </dependency>

            <!-- JACKSON -->
            <dependency>
                <groupId>com.fasterxml.jackson</groupId>
                <artifactId>jackson-bom</artifactId>
                <version>${jackson.version}</version>
                <scope>import</scope>
                <type>pom</type>
            </dependency>

            <dependency>
                <groupId>org.slf4j</groupId>
                <artifactId>slf4j-api</artifactId>
                <version>${slf4j.version}</version>
            </dependency>
        </dependencies>
    </dependencyManagement>

    <build>
        <extensions>
            <extension>
                <groupId>ar.com.synergian</groupId>
                <artifactId>wagon-git</artifactId>
                <version>${wagon-git.version}</version>
            </extension>
        </extensions>

        <plugins>
            <!-- https://maven.apache.org/wrapper/index.html -->
            <!-- https://gradle.com/blog/use-the-maven-wrapper-to-optimize-your-build-workflow/ -->
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-wrapper-plugin</artifactId>
                <version>${maven-wrapper-plugin.version}</version>
            </plugin>

            <!-- https://github.com/outbrain/ci-friendly-flatten-maven-plugin -->
            <!-- https://medium.com/outbrain-engineering/faster-release-with-maven-ci-friendly-versions-and-a-customised-flatten-plugin-fe53f0fcc0df -->
            <plugin>
                <groupId>com.outbrain.swinfra</groupId>
                <artifactId>ci-friendly-flatten-maven-plugin</artifactId>
                <version>${ci-friendly-flatten-maven-plugin.version}</version>
                <executions>
                    <execution>
                        <goals>
                            <!-- Ensure proper cleanup. Will run on clean phase-->
                            <goal>clean</goal>
                            <!-- Enable ci-friendly version resolution. Will run on process-resources phase-->
                            <goal>flatten</goal>
                        </goals>
                    </execution>
                </executions>
            </plugin>
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-toolchains-plugin</artifactId>
                <version>${maven-toolchains-plugin.version}</version>
                <executions>
                    <execution>
                        <goals>
                            <goal>toolchain</goal>
                        </goals>
                    </execution>
                </executions>
                <configuration>
                    <toolchains>
                        <jdk>
                            <version>${java.version}</version>
                            <vendor>openjdk</vendor>
                        </jdk>
                    </toolchains>
                </configuration>
            </plugin>

            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-source-plugin</artifactId>
                <version>${maven-source-plugin.version}</version>
                <executions>
                    <execution>
                        <id>attach-sources</id>
                        <phase>verify</phase>
                        <goals>
                            <goal>jar-no-fork</goal>
                        </goals>
                    </execution>
                </executions>
            </plugin>
        </plugins>

        <pluginManagement>
            <plugins>
                <!-- COMPILER -->
                <plugin>
                    <groupId>org.apache.maven.plugins</groupId>
                    <artifactId>maven-compiler-plugin</artifactId>
                    <version>${maven-compiler-plugin.version}</version>
                    <configuration>
                        <release>${java.version}</release>
                        <source>${java.version}</source>
                        <target>${java.version}</target>
                        <enablePreview>true</enablePreview>
                        <compilerArgs>--enable-preview</compilerArgs>
                        <compilerReuseStrategy>allwaysNew</compilerReuseStrategy>
                        <annotationProcessorPaths>
                            <annotationProcessorPath>
                                <groupId>org.projectlombok</groupId>
                                <artifactId>lombok</artifactId>
                                <version>${lombok.version}</version>
                            </annotationProcessorPath>
                        </annotationProcessorPaths>
                    </configuration>
                </plugin>

                <!-- BUILD HELPER -->
                <plugin>
                    <groupId>org.codehaus.mojo</groupId>
                    <artifactId>build-helper-maven-plugin</artifactId>
                    <version>${build-helper-plugin.version}</version>
                </plugin>
            </plugins>
        </pluginManagement>
    </build>

    <repositories>
        <repository>
            <id>central1</id>
            <name>Maven Central Repository</name>
            <url>https://repo1.maven.org/maven2/</url>
        </repository>

        <!-- jaxb2-basics-plugins JAXB3 -->
        <!-- https://github.com/McBluna/jaxb3-basics/tree/master -->
        <repository>
            <id>mcbluna.net</id>
            <url>https://mcbluna.net/repository/</url>
        </repository>
    </repositories>

    <pluginRepositories>
        <pluginRepository>
            <id>synergian-repo</id>
            <url>https://raw.github.com/synergian/wagon-git/releases</url>
        </pluginRepository>
    </pluginRepositories>

    <distributionManagement>
        <repository>
            <id>kumo-releases</id>
            <name>kumo releases repo</name>
            <url>git:releases://git@bitbucket.org:seguradev/kumo-maven-repository.git</url>
        </repository>
        <snapshotRepository>
            <id>kumo-snapshots</id>
            <name>kumo snapshots repo</name>
            <url>git:snapshots://git@bitbucket.org:seguradev/kumo-maven-repository.git</url>
        </snapshotRepository>
        <!-- <site>
            <id>${project.artifactId}-site</id>
            <url>${project.baseUri}</url>
        </site> -->
    </distributionManagement>

    <scm>
        <connection>scm:git:ssh://git@bitbucket.org:seguradev/kumo-corev2.git</connection>
        <developerConnection>scm:git:ssh://git@bitbucket.org:seguradev/kumo-corev2.git</developerConnection>
        <url>https://bitbucket.org/seguradev/kumo-corev2</url>
        <tag>HEAD</tag>
    </scm>

    <ciManagement>
        <system>jenkins</system>
        <url>https://jenkins.fsegura.com/</url>
    </ciManagement>

    <organization>
        <name>Grupo Segura</name>
        <url>https://www.fsegura.com/</url>
    </organization>

    <developers>
        <developer>
            <id>rialonso</id>
            <name>Ricardo Alonso</name>
            <email>rialonso@fsegura.com</email>
            <organization>Grupo Segura</organization>
        </developer>
    </developers>
</project>