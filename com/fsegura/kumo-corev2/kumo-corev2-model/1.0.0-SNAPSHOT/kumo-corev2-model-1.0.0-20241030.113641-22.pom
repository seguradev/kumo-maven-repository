<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0"
         xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
    <modelVersion>4.0.0</modelVersion>
    <parent>
        <groupId>com.fsegura.kumo-corev2</groupId>
        <artifactId>kumo-corev2</artifactId>
        <version>1.0.0-SNAPSHOT</version>
    </parent>

    <artifactId>kumo-corev2-model</artifactId>
    <name>kumo-corev2-model</name>
    <packaging>jar</packaging>

    <properties>

        <!-- PLUGINS -->
        <apt-maven-plugin.version>1.1.3</apt-maven-plugin.version>

        <generated-sources.path>${project.build.directory}/generated-sources/java</generated-sources.path>
    </properties>

    <dependencies>
        <dependency>
            <groupId>org.hibernate.orm</groupId>
            <artifactId>hibernate-core</artifactId>
            <version>${hibernate.version}</version>
        </dependency>
        <dependency>
            <groupId>org.hibernate.orm</groupId>
            <artifactId>hibernate-envers</artifactId>
            <version>${hibernate.version}</version>
            <exclusions>
                <exclusion>
                    <groupId>org.hibernate.orm</groupId>
                    <artifactId>hibernate-core</artifactId>
                </exclusion>
            </exclusions>
        </dependency>

        <dependency>
            <groupId>io.hypersistence</groupId>
            <artifactId>hypersistence-utils-hibernate-63</artifactId>
            <version>${hypersistence-utils.version}</version>
            <!--<optional>true</optional>-->
        </dependency>

        <dependency>
            <groupId>jakarta.persistence</groupId>
            <artifactId>jakarta.persistence-api</artifactId>
        </dependency>

        <dependency>
            <groupId>com.fsegura.kumo-corev2</groupId>
            <artifactId>kumo-corev2-common</artifactId>
            <version>${project.version}</version>
        </dependency>
        <dependency>
            <groupId>com.fsegura.kumo-corev2</groupId>
            <artifactId>kumo-corev2-data</artifactId>
            <version>${project.version}</version>
        </dependency>
        <dependency>
            <groupId>com.fsegura.kumo-corev2</groupId>
            <artifactId>kumo-corev2-data-querydsl</artifactId>
            <version>${project.version}</version>
        </dependency>
        <dependency>
            <groupId>com.fsegura.kumo-corev2</groupId>
            <artifactId>kumo-corev2-model-common</artifactId>
            <version>${project.version}</version>
        </dependency>
        <dependency>
            <groupId>com.fsegura.kumo-corev2</groupId>
            <artifactId>kumo-corev2-sync-vo</artifactId>
            <version>${project.version}</version>
        </dependency>

        <dependency>
            <groupId>com.querydsl</groupId>
            <artifactId>querydsl-core</artifactId>
            <version>${querydsl.version}</version>
        </dependency>
        <dependency>
            <groupId>com.querydsl</groupId>
            <artifactId>querydsl-jpa</artifactId>
            <version>${querydsl.version}</version>
            <classifier>jakarta</classifier>
        </dependency>

        <!-- SpotBugs -->
        <!--<dependency>
            <groupId>com.github.spotbugs</groupId>
            <artifactId>spotbugs-annotations</artifactId>
            <version>${spotbugs-annotations.version}</version>
        </dependency>-->
    </dependencies>

    <dependencyManagement>
        <dependencies>
            <dependency>
                <groupId>com.querydsl</groupId>
                <artifactId>querydsl-bom</artifactId>
                <version>${querydsl.version}</version>
                <scope>import</scope>
                <type>pom</type>
            </dependency>

        </dependencies>
    </dependencyManagement>

    <build>
        <resources>
            <resource>
                <directory>src/main/resources</directory>
                <filtering>true</filtering>
                <includes>
                    <include>**/*.sql</include>
                </includes>
                <!-- <excludes>
                    <exclude>**/*.properties</exclude>
                </excludes> -->
            </resource>
            <resource>
            <directory>src/main/resources</directory>
            <filtering>false</filtering>
                <includes>
                    <include>**/*.xml</include>
                </includes>
            </resource>
        </resources>

        <plugins>
            <!-- <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-compiler-plugin</artifactId>
                <configuration>
                    <generatedSourcesDirectory>${generated-sources.path}</generatedSourcesDirectory>
                    <annotationProcessorPaths>
                        <annotationProcessorPath>
                            <groupId>org.projectlombok</groupId>
                            <artifactId>lombok</artifactId>
                            <version>${lombok.version}</version>
                        </annotationProcessorPath>
                        <annotationProcessorPath>
                            <groupId>com.querydsl</groupId>
                            <artifactId>querydsl-apt</artifactId>
                            <version>${querydsl.version}</version>
                            <classifier>jakarta</classifier>
                        </annotationProcessorPath>
                    </annotationProcessorPaths>
                    <compilerArgs>
                        <processor>com.querydsl.apt.jpa.JPAAnnotationProcessor</processor>
                        <processor>lombok.launch.AnnotationProcessorHider$AnnotationProcessor</processor>
                        <compilerArg>-Aquerydsl.entityAccessors=true</compilerArg>
                        <compilerArg>-Aquerydsl.listAccessors=true</compilerArg>
                        <compilerArg>-Aquerydsl.mapAccessors=true</compilerArg>
                        <compilerArg>-Aquerydsl.generatedAnnotationClass=jakarta.annotation.Generated</compilerArg>
                    </compilerArgs>
                </configuration>
            </plugin> -->

            <!-- QUERY DSL GENERATOR -->
            <plugin>
                <groupId>com.mysema.maven</groupId>
                <artifactId>apt-maven-plugin</artifactId>
                <version>${apt-maven-plugin.version}</version>
                <dependencies>
                    <dependency>
                        <groupId>com.querydsl</groupId>
                        <artifactId>querydsl-apt</artifactId>
                        <version>${querydsl.version}</version>
                        <classifier>jakarta</classifier>
                    </dependency>
                </dependencies>
                <configuration>
                    <compilerOptions>
                        <source>${java.version}</source>
                        <deprecation>--enable-preview</deprecation>
                    </compilerOptions>
                </configuration>
                <executions>
                    <execution>
                        <phase>generate-sources</phase>
                        <goals>
                            <goal>process</goal>
                        </goals>
                        <configuration>
                            <outputDirectory>${generated-sources.path}</outputDirectory>
                            <processors>
                                <processor>com.querydsl.apt.jpa.JPAAnnotationProcessor</processor>
                                <processor>lombok.launch.AnnotationProcessorHider$AnnotationProcessor</processor>
                            </processors>
                            <options>
                                <querydsl.entityAccessors>true</querydsl.entityAccessors>
                                <querydsl.listAccessors>true</querydsl.listAccessors>
                                <querydsl.mapAccessors>true</querydsl.mapAccessors>
                                <querydsl.generatedAnnotationClass>jakarta.annotation.Generated</querydsl.generatedAnnotationClass>
                            </options>
                        </configuration>
                    </execution>
                </executions>
            </plugin>

            <!-- BUILD HELPER -->
            <plugin>
                <groupId>org.codehaus.mojo</groupId>
                <artifactId>build-helper-maven-plugin</artifactId>
                <executions>
                    <execution>
                        <id>add-source</id>
                        <phase>generate-sources</phase>
                        <goals>
                            <goal>add-source</goal>
                        </goals>
                    </execution>
                </executions>
                <configuration>
                    <sources>
                        <!-- <source>${mapstruct.generated}</source> -->
                        <source>${project.build.directory}/generated-sources/java</source>
                    </sources>
                </configuration>
            </plugin>
        </plugins>
    </build>

</project>