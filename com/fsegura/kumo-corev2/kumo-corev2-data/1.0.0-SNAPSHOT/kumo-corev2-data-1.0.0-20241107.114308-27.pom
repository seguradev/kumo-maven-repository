<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0"
         xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
    <modelVersion>4.0.0</modelVersion>
    <parent>
        <groupId>com.fsegura.kumo-corev2</groupId>
        <artifactId>kumo-corev2</artifactId>
        <version>1.0.0-SNAPSHOT</version>
    </parent>

    <artifactId>kumo-corev2-data</artifactId>

    <properties>
        <generated-sources.path>${project.build.directory}/generated-sources/java</generated-sources.path>
    </properties>

    <dependencies>
        <dependency>
            <groupId>org.hibernate.orm</groupId>
            <artifactId>hibernate-core</artifactId>
            <version>${hibernate.version}</version>
            <scope>compile</scope>
            <optional>true</optional>
        </dependency>

        <dependency>
            <groupId>org.hibernate.validator</groupId>
            <artifactId>hibernate-validator</artifactId>
            <version>${hibernate-validator.version}</version>
        </dependency>

        <dependency>
            <groupId>org.springframework.data</groupId>
            <artifactId>spring-data-jpa</artifactId>
        </dependency>

        <dependency>
            <groupId>jakarta.persistence</groupId>
            <artifactId>jakarta.persistence-api</artifactId>
        </dependency>

        <dependency>
            <groupId>jakarta.xml.bind</groupId>
            <artifactId>jakarta.xml.bind-api</artifactId>
        </dependency>

        <dependency>
            <groupId>${project.groupId}</groupId>
            <artifactId>kumo-corev2-util</artifactId>
            <version>${project.version}</version>
            <scope>compile</scope>
            <optional>true</optional>
        </dependency>

        <dependency>
            <groupId>com.querydsl</groupId>
            <artifactId>querydsl-core</artifactId>
            <version>${querydsl.version}</version>
            <optional>true</optional>
        </dependency>
        <dependency>
            <groupId>com.querydsl</groupId>
            <artifactId>querydsl-jpa</artifactId>
            <version>${querydsl.version}</version>
            <classifier>jakarta</classifier>
            <optional>true</optional>
            <exclusions>
                <exclusion>
                    <groupId>com.querydsl</groupId>
                    <artifactId>querydsl-core</artifactId>
                </exclusion>
            </exclusions>
        </dependency>
    </dependencies>

    <build>
        <plugins>
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-compiler-plugin</artifactId>
                <configuration>
                    <generatedSourcesDirectory>${generated-sources.path}</generatedSourcesDirectory>
                    <annotationProcessorPaths>
                        <annotationProcessorPath>
                            <groupId>org.projectlombok</groupId>
                            <artifactId>lombok</artifactId>
                            <version>${lombok.version}</version>
                        </annotationProcessorPath>
                        <annotationProcessorPath>
                            <groupId>org.hibernate.orm</groupId>
                            <artifactId>hibernate-jpamodelgen</artifactId>
                            <version>${hibernate.version}</version>
                        </annotationProcessorPath>
                        <annotationProcessorPath>
                            <groupId>com.querydsl</groupId>
                            <artifactId>querydsl-apt</artifactId>
                            <version>${querydsl.version}</version>
                            <classifier>jakarta</classifier>
                        </annotationProcessorPath>
                    </annotationProcessorPaths>
                    <compilerArgs>
                        <processor>org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor</processor>
                        <processor>com.querydsl.apt.jpa.JPAAnnotationProcessor</processor>
                        <processor>lombok.launch.AnnotationProcessorHider$AnnotationProcessor</processor>
                        <compilerArg>-Aquerydsl.entityAccessors=true</compilerArg>
                        <compilerArg>-Aquerydsl.listAccessors=true</compilerArg>
                        <compilerArg>-Aquerydsl.mapAccessors=true</compilerArg>
                        <compilerArg>-Aquerydsl.generatedAnnotationClass=jakarta.annotation.Generated</compilerArg>
                    </compilerArgs>
                </configuration>
            </plugin>

            <!-- BUILD HELPER -->
            <plugin>
                <groupId>org.codehaus.mojo</groupId>
                <artifactId>build-helper-maven-plugin</artifactId>
                <executions>
                    <execution>
                        <id>add-source</id>
                        <phase>generate-sources</phase>
                        <goals>
                            <goal>add-source</goal>
                        </goals>
                    </execution>
                </executions>
                <configuration>
                    <sources>
                        <source>${generated-sources.path}</source>
                    </sources>
                </configuration>
            </plugin>

            <!-- QUERY DSL GENERATOR -->
            <!-- <plugin>
                <groupId>com.mysema.maven</groupId>
                <artifactId>apt-maven-plugin</artifactId>
                <version>${apt-maven-plugin.version}</version>
                <dependencies>
                    <dependency>
                        <groupId>org.hibernate.orm</groupId>
                        <artifactId>hibernate-jpamodelgen</artifactId>
                        <version>${hibernate.version}</version>
                    </dependency>
                </dependencies>
                <executions>
                    <execution>
                        <phase>generate-sources</phase>
                        <goals>
                            <goal>process</goal>
                        </goals>
                        <configuration>
                            <outputDirectory>${project.build.directory}/generated-sources/java</outputDirectory>
                            <processor>org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor</processor>
                        </configuration>
                    </execution>
                </executions>
            </plugin> -->
        </plugins>
    </build>

</project>